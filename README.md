# Django

## Configurer la base de donnée postgres (requis)

1) Copier le fichier django/Sonar/example.env et le nommer .env
2) `cp django/Sonar/example.env django/Sonar/.env`
3) Remplacer les valeurs par celles désirées
4) Ce fichier sera lu:
    - par le container docker de la base de donnée pour créer la BDD
    - par le fichier settings.py de django pour que l'application se connecte à la BDD
5) Lancer le docker : `sudo docker-compose up -d`

## Installer le schéma de base de donnée et les données initiales (requis)

### Installation du schéma

Ouvrir la console du container django:
- `sudo docker container ls ` Voir l’ID des containers
- `sudo docker exec -it <ID_DU_CONTAINER_DJANGO> bash`

Puis éxécuter:
- `python manage.py migrate`

### Installation des données initiales

Les données pour initialiser l'application django sont contenues dans des fixtures.
https://docs.djangoproject.com/fr/3.1/howto/initial-data/

La plupart de ces fixtures sont installées automatiquement par le fichier `entrypoint.sh`, au moment où le containeur django est lancé.

Cependant, d'autres données initiales de tailles plus importantes doivent être installées manuellement:
- Les localisations (import de donnée d'OpenStreetMap)
- Les actions des boulangeries

Pour les installer, éxécuter les commandes suivantes (dans le containeur django) :
- `python manage.py import_osm_data ` -> Ajoute les localisations. Cette étape peut prendre entre 5 et 10 minutes
- `python manage.py start_bakery_action ` -> L'initialise l'action des boulangeries. **ATTENTION**, cette commande ne doit pas être éxécutée plusieurs fois

## Configurer une application et un bot discord (requis)

L'authentification est déléguée à Discord, cependant les comptes discord et les rôles du serveur sont dupliqués dans l'application Django. Pour faire fonctionner cette duplication, il faut configurer les variables d'environnement suivantes (dans le fichier .env). Les valeurs sont disponibles dans une note sécurisée `Variables d'environement Discord` du coffre La carte de BitWarden : https://keys.projet-meduses.com/#/vault?collectionId=ab3f4163-4ff1-4fb0-bec9-a8f6e74c01fb

- `DISCORD_CLIENT_ID`
- `DISCORD_CLIENT_SECRET`
- `DISCORD_KEY`
- `DISCORD_GUILD_ID`
- `DISCORD_BOT_TOKEN`

## Lancer l'environnement de développement

Pour lancer l'environnement de développement, éxécuter la commande suivante:
- `bash start.dev.sh`

Cette commande lancera les fichiers `docker-compose.yml` et `docker-compose.dev.yml` en les mergeant.

Plus d'informations à propos de l'utilisation de plusieurs fichiers `docker-compose`: https://docs.docker.com/compose/reference/overview/#specifying-multiple-compose-files

## Lancer l'environnement de production

Pour lancer l'environnement de production, éxécuter la commande suivante:
- `bash start.sh`

Cette commande lancera les fichiers `docker-compose.yml` et `docker-compose.prod.yml` en les mergeant, [en mode détaché](https://docs.docker.com/compose/reference/up/).

## API Endpoints

### Authentifier une requête

Pour authentifier une requête celle-ci doit contenir un token `Authorization` dans ses headers. La valeur de se header doit commencer par `JWT` suivi d'un espace et du token d'authentification. Exemple:
```json
{
    "Authorization": "JWT <token>"
}
```
Deux méthodes existent pour obtenir ce token:
- Utilisateur django avec un mot de passe:
    - `/auth/token/obtain` [POST] Renvoit un token.
        - Exemple de payload pour récupérer un token:
        ```json
        {
            "username": "PetiteMeduse",
            "password": "tentacula"
        }
        ```
    - `/auth/token/refresh` [POST] Renouvelle un token.
        - Exemple de payload pour renouveler un token:
        ```json
        {
            "token": "<token>"
        }
        ```
- Utilisateur importé de Discord, sans mot de passe:
    - `/auth/token/exchange` [POST] Echange un code Oath discord avec un Token.
        - Le code Oath est renvoyé en paramètre GET dans le callback de l'authentification Discord.
        - Voir dans `react/pages/login.tsx` pour un exemple d'implémentation.
        - Exemple de payload pour récupérer un token:
        ```json
        {
            "code": "<code>"
        }
        ```
    - Le token peut-être renouvellé de la même manière que pour un utilisateur avec mot de passe.

### Endpoints

- `/api/localisations_types/` [GET] Liste des types de localisations
- `/api/localisations_types/<id>/` [GET] Retourne le détail d'un type de localisation
- `/api/localisations` [GET] Liste des localisations. Arguments supportés :
    - [tile] Filtre les résultats par tuile (https://github.com/openwisp/django-rest-framework-gis#tmstilefilter)
    - Exemple: `/api/localisations?tile=7/64/44`
- `/api/localisations/<id>` [GET] Retourne le détail d'une localisation
- `/api/action_dones/` [GET, POST] Liste des validations d'action.
    - Exemple de payload pour ajouter une validation d'action:
    ```json
    {
        "comment": "Propriétaires très sympas !",
        "action": 1,
        "localisation": 1
    }
    ```
- `/api/action_dones/1/` [GET] Returne le détail d'une validation d'action