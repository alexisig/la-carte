from django.contrib.auth.models import Group
from rest_framework import permissions
from .constants import MEDUSE_ROLE_NAME

def is_in_group(user, group_name):
    """
    Takes a user and a group name, and returns `True` if the user is in that group.
    """
    try:
        return Group.objects.get(name=group_name).user_set.filter(id=user.id).exists()
    except Group.DoesNotExist:
        return False

class MeduseAndStaffOnlyPermission(permissions.IsAuthenticated):
    """Ensure user is a Méduse or an admin"""

    def has_permission(self, request, view):
        is_authenticated = super(MeduseAndStaffOnlyPermission, self).has_permission(request, view)
        user = request.user
        return (
            user.is_staff or is_in_group(user, MEDUSE_ROLE_NAME)
        ) and is_authenticated