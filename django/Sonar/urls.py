"""Sonar URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.contrib import admin
from django.urls import path, include, reverse_lazy
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from .views import exchange_token
from django.views.generic.base import RedirectView
from django.conf.urls.static import static

urlpatterns = [
    path('api/', include('goingson.urls')),
    path('admin/clearcache/', include('clearcache.urls')),
    path('admin/', admin.site.urls),
    path('auth/token/exchange', exchange_token),
    path('auth/token/obtain', obtain_jwt_token),
    path('auth/token/refresh', refresh_jwt_token),
    path('auth/token/verify', verify_jwt_token),
    path('accounts/', include('allauth.urls')),
    path('', RedirectView.as_view(url=reverse_lazy('admin:index'))),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
