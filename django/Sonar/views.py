from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.models import User
from rest_framework_jwt.settings import api_settings
from allauth.socialaccount.models import SocialAccount
from django.core.management import call_command
from discord_users.models import UserAvatar
from goingson.serializers import UserSerializer

from Sonar.settings import REACT_BASE_URL

import requests
import os

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

DISCORD_API_BASE_URL = 'https://discord.com/api/v6'

def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'user': UserSerializer().to_representation(user)
    }

@api_view(['POST'])
@permission_classes([AllowAny])
def exchange_token(request):
    code = str(request.data['code'])
    
    if not code:
        return Response({
            'message': 'Code is missing from payload.',
            'error': 'missing_code'
        }, status=status.HTTP_400_BAD_REQUEST)
    
    payload = {
        'client_id': os.getenv('DISCORD_CLIENT_ID'),
        'client_secret': os.getenv('DISCORD_CLIENT_SECRET'),
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': f'{REACT_BASE_URL}/login',
        'scope': 'identify,email'
    }
    response = requests.post('https://discord.com/api/oauth2/token', data=payload)
    credentials = response.json()

    if not 'access_token' in credentials:
        return Response({
            'message': 'Could not retrieve access token from discord oauth2.',
            'error': 'cannot_get_discord_access_token',
            'details': credentials
        }, status=status.HTTP_400_BAD_REQUEST)

    access_token = credentials['access_token']

    headers = { 'Authorization': f'Bearer {access_token}' }

    identify_response = requests.get(f'{DISCORD_API_BASE_URL}/users/@me', headers=headers)
    identity = identify_response.json()

    social_account = SocialAccount.objects.filter(uid=identity['id']).first()

    if not social_account:
        return Response({
            'message': 'User doesnt exist',
            'error': 'missing_user'
        }, status=status.HTTP_404_NOT_FOUND)
    
    user = social_account.user

    avatar = None
    if identity['avatar']:
        avatar = f'https://cdn.discordapp.com/avatars/{social_account.uid}/{identity["avatar"]}.png'

    UserAvatar.objects.update_or_create(
        user=user,
        defaults={ 'url': avatar }
    )

    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)

    call_command('import_discord_roles')

    return Response(
        jwt_response_payload_handler(token, user),
        status=status.HTTP_200_OK
    )
