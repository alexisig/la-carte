from django.contrib import admin
from .models import UserAvatar
from allauth.socialaccount.models import SocialToken, SocialAccount, SocialApp
from django.contrib.sites.models import Site
from allauth.account.models import EmailAddress

admin.site.register(UserAvatar)

admin.site.unregister(SocialToken)
admin.site.unregister(SocialAccount)
admin.site.unregister(SocialApp)
admin.site.unregister(EmailAddress)
