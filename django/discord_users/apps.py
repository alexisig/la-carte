from django.apps import AppConfig


class DiscordGroupsConfig(AppConfig):
    name = 'discord_users'
    verbose_name = 'Utilisateurs Discord'
