from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import Permission, Group
from Sonar.constants import CAMPAIGNER_ROLE_NAME

class Command(BaseCommand):    
    def handle(self, *args, **options):
        group_permissions = {
            'add': [
                'action',
                'actioncollection',
                'localisation',
                'localisationtype',
                'localisationactioncollectionstatus'
            ], 
            'change': [
                'action',
                'actioncollection',
                'localisation',
                'localisationtype',
                'localisationactioncollectionstatus',
                'user'

            ],
            'delete': [
                'actioncollection',
                'localisation',
                'localisationtype',
                'localisationactioncollectionstatus'
            ],
            'view': [
                'action',
                'actioncollection',
                'actiondone',
                'localisation',
                'localisationtype',
                'localisationactioncollectionstatus',
                'user'
            ]
        }

        group_name = CAMPAIGNER_ROLE_NAME
        group, created = Group.objects.get_or_create(name=group_name)
        group.permissions.clear()

        for action in group_permissions:
            for model_name in group_permissions[action]:
                codename = action + '_' + model_name
                group.permissions.add(Permission.objects.get(codename=codename))
                print(f'Adding permission {codename} to group "{group_name}"')

