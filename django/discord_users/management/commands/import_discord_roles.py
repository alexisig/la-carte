from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import Group, User
from allauth.socialaccount.models import SocialAccount
from Sonar.constants import CAMPAIGNER_ROLE_NAME

import discord
import os
import asyncio

class Command(BaseCommand):
    help = 'Create Django groups based on discord roles and assign to django users.'
    
    def handle(self, *args, **options):
        '''

        For this command to work, it is necessary to activate the following options in the discord bot panel:
        - Privileged Gateway Intents -> Server Members Intent

        '''
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        intents = discord.Intents()
        intents.members = True
        client = discord.Client(intents=intents)

        loop.run_until_complete(client.login(os.getenv('DISCORD_BOT_TOKEN')))
        guild = loop.run_until_complete(client.fetch_guild(os.getenv('DISCORD_GUILD_ID')))
        roles = loop.run_until_complete(guild.fetch_roles())
        members = loop.run_until_complete(guild.fetch_members().flatten())

        for member in members:
            social_account = SocialAccount.objects.filter(uid=member.id).first()

            if not social_account:
                continue

            user = social_account.user
            
            # Remove all groups from user and readd the one they currently have
            
            user_is_campaigner = False

            if user.groups.filter(name=CAMPAIGNER_ROLE_NAME).exists():
                user_is_campaigner = True
            
            user.groups.clear()

            if user_is_campaigner:
                user.groups.add(Group.objects.get(name=CAMPAIGNER_ROLE_NAME))
            
            for role in member.roles:
                group, created = Group.objects.get_or_create(name=role.name)

                if created:
                    print(f'New role "{role.name}" was successfully imported to django.')
                
                user.groups.add(group)
            
        loop.run_until_complete(client.close())