from django.contrib.auth.models import User
from django.db import models

class UserAvatar(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    url = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.user.username