#!/bin/bash

pip install -r requirements.txt --default-timeout=100
python manage.py migrate
python manage.py collectstatic --no-input
python manage.py create_campaigner_group
python manage.py loaddata goingson/fixtures/localisationtype.json
python manage.py loaddata goingson/fixtures/actioncollection.json
python manage.py loaddata goingson/fixtures/action.json
python manage.py loaddata goingson/fixtures/actioncollectionstatus.json
python manage.py import_discord_roles

if [ -n "$1" ]
then
  if [ "$1" == "dev" ]
  then
    python manage.py runserver 0.0.0.0:8000 &
    find . -name '*.py' | entr python ./manage.py test goingson --parallel --keepdb

    # Note : Tests should not be run on production as the test engine
    # use the same cache interface for the live databse and the test
    # database

  elif [ "$1" == "prod" ]
  then
    uwsgi --ini uwsgi.ini
  fi
fi