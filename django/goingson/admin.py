from django.contrib import admin
from django.contrib.gis.admin import OSMGeoAdmin
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from .models import Action, Localisation, \
    LocalisationType, ActionDone, \
    ActionCollectionStatus, LocalisationActionCollectionStatus, \
    ActionCollection

from Sonar.constants import CAMPAIGNER_ROLE_NAME
from Sonar.settings import REACT_BASE_URL

import os

class GroupBasedAdminSite(admin.AdminSite):
    def has_permission(self, request):
        return request.user.is_active and (
            request.user.groups.filter(name=CAMPAIGNER_ROLE_NAME).exists()
            or 
            request.user.is_superuser
        )

admin.site = GroupBasedAdminSite()

class EfficientAdminWithLocalisation(admin.ModelAdmin):
    raw_id_fields = ('localisation',)

class ActionDoneAdmin(EfficientAdminWithLocalisation):
    readonly_fields = ('date',)

class LocalisationActionCollectionStatusAdmin(EfficientAdminWithLocalisation):
    readonly_fields = ('current_action',)

class LocalisationAdmin(OSMGeoAdmin):
    readonly_fields = ('osm_url',)
    search_fields = ('tags', 'id', 'osm_id')

class ActionCollectionAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    readonly_fields = ('actions_admin_str',)

class ActionAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    filter_horizontal = ('permission_to_complete',)

class LocalisationTypeAdmin(admin.ModelAdmin):
    search_fields = ('name', 'display_name',)


admin.site.register(User, UserAdmin)
admin.site.register(Group, GroupAdmin)

admin.site.register(Action, ActionAdmin)
admin.site.register(Localisation, LocalisationAdmin)
admin.site.register(LocalisationType, LocalisationTypeAdmin)
admin.site.register(ActionDone, ActionDoneAdmin)
admin.site.register(LocalisationActionCollectionStatus, LocalisationActionCollectionStatusAdmin)
admin.site.register(ActionCollectionStatus)
admin.site.register(ActionCollection, ActionCollectionAdmin)

admin.site.site_header = 'Projet Méduses - La Carte'
admin.site.site_title = 'La Carte'
admin.site.index_title = 'Admin'

admin.site.site_url = REACT_BASE_URL

admin.site.index_template = 'memcache_status/admin_index.html'