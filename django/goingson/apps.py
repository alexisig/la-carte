from django.apps import AppConfig


class GoingsOnConfig(AppConfig):
    name = 'goingson'
    verbose_name = 'Données'
    path = './goingson'
