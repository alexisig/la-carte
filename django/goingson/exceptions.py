from rest_framework.exceptions import APIException

class ActionNotRepeatable(APIException):
    status_code = 429
    default_detail = 'Action can not be repeated right now, try again later.'
    default_code = 'action_not_repeatable'