from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point

from goingson.models import Localisation, LocalisationType
from shapely.geometry import Polygon

from alive_progress import alive_bar 
from lazyme.string import color_print

import requests
import os
import traceback
import json

'''
Some areas' id for overpass:

Paris: 3600007444
France: 3602202162

This can be changed in the .env file.
'''

OVERPASS_ENDPOINT = 'https://overpass.alexisig.com/api/interpreter' 
# i use my personnal overpass instance to avoid timeouts, frequent on official overpass instances

def get_area_osm_id(area_id):
    prefix, osm_id = area_id.split('360')
    return osm_id

NOMINATIM_ENDPOINT = f'https://nominatim.openstreetmap.org/details.php?osmtype=R&osmid={get_area_osm_id(os.getenv("OVERPASS_IMPORT_AREA"))}&format=json'

ENTITIES = [
    {
        'name': 'bakery',
        'query': f'''
            [out:json][timeout:9999];
            area({os.getenv('OVERPASS_IMPORT_AREA')})->.searchArea;
            (
                node["shop"="bakery"](area.searchArea);
                way["shop"="bakery"](area.searchArea);
            );
            out body;
            >;
            out skel qt;
        '''
    }
]

def is_poi_part_of_way(poi):
    return 'tags' not in poi

def is_poi_way(poi):
    return poi['type'] == 'way'

def is_poi_node(poi):
    return 'lat' in poi and not is_poi_part_of_way(poi)

class Command(BaseCommand):
    help = 'Upsert OSM data with the overpass API'

    def handle(self, *args, **options):
        for entity in ENTITIES:
            localisation_type = LocalisationType.objects.filter(name=entity['name']).first()
            if not localisation_type:
                name = entity['name']
                print(f'Cannot find localisation type {name}. Skipping.')
                continue
            
            def update_or_create(POI, point, type):
                '''

                Checks if a localisation already exists in the database using the OSM unique id of the entity and :
                - Updates it if it already exists
                - Adds it if it doesn't

                '''
                Localisation.objects.update_or_create(
                        osm_id=POI['id'],
                        defaults={
                            'type': localisation_type,
                            'geom': point,
                            'tags': POI['tags']
                        }
                    )

            print('Area to request:')
            response = requests.get(NOMINATIM_ENDPOINT)
            print(response.json()['localname'])

            print('Requesting data to overpass (this may take a while) ...')
            try:
                with alive_bar() as bar:
                    response = requests.post(url=OVERPASS_ENDPOINT, data=entity['query'])
                    response_json = response.json()
            except Exception:
                color_print('ERROR. Requesting data from overpass failed', color='red')
                print(traceback.print_exc())
                exit()
            
            print('Request to overpass successfull')

            ways = []
            nodes_for_ways = []

            '''

            Ways returned by overpass need their geometry (polygon) to be
            reconstructed from the nodes that they are made of.

            Overpass returns the list of ids of these nodes when querying
            for a way, and the nodes themselves are returned separately.

            The code below identify the nodes that are part of ways
            (those without tags), set them aside, and then use them to
            reconstruct the geometry of the ways (polygon).
            
            To normalize the data, the way's geometries are then transformed
            into points using their centroid.
            
            '''

            print('Upsertting nodes to database ...')

            with alive_bar(
                len(list(filter(lambda e: is_poi_node(e), response_json['elements'])))
            ) as bar:
                for POI in response_json['elements']:
                    if is_poi_part_of_way(POI):
                        nodes_for_ways.append(POI)
                    elif is_poi_way(POI):
                        ways.append(POI)
                    else:
                        update_or_create(POI, Point(x=POI['lon'], y=POI['lat']), 'node')
                        bar()
            
            print('Upsertting ways to database ...')

            with alive_bar(len(ways)) as bar:
                for way in ways:
                    nodes = filter(lambda node: node['id'] in way['nodes'], nodes_for_ways)
                    nodes_coords = map(lambda node: [node['lon'], node['lat']], nodes)
                    centroid = list(Polygon(nodes_coords).centroid.coords)[0]
                    update_or_create(way, Point(centroid), 'way')
                    bar()

            color_print(f'SUCCESS. Done importing "{localisation_type.name}" data from Overpass', color='green')

            
