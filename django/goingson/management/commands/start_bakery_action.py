from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point

from goingson.models import Localisation, LocalisationType, LocalisationActionCollectionStatus, ActionCollection, Action, ActionCollectionStatus
from shapely.geometry import Polygon

from alive_progress import alive_bar
from lazyme.string import color_print

import requests
import os


class Command(BaseCommand):
    help = 'Create an instance of LocalisationActionCollectionStatus for all bakeries that meet the start condition criteria.'

    def localisation_meet_the_start_condition_criteria(self, localisation):
        return localisation.is_vegan() == False and localisation.is_vegan_friendly() == False

    def handle(self, *args, **options):
        confirmation = input('''
WARNING: This will reset all the status
for the localisation that have already
started this action.

This command is intended to be run only once.

Do you want to continue? [y/N]
        ''')
        if confirmation != 'y':
            return
        
        bakery_type = LocalisationType.objects.get(name='bakery')
        bakeries = Localisation.objects.filter(type=bakery_type).all()
        action_collection = ActionCollection.objects.get(pk=1)
        first_action = Action.objects.filter(collection=action_collection) \
            .order_by('order_in_collection')[0]

        status_started = ActionCollectionStatus.objects.get(name='started')
        status_start_condition_not_respected = ActionCollectionStatus.objects.get(name='start_condition_not_respected')
        
        count_started = 0
        count_start_condition_not_respected = 0

        print('Starting the action in the selected bakeries ...')

        with alive_bar(len(bakeries)) as bar:
            for bakery in bakeries:
                start_condition_respected = self.localisation_meet_the_start_condition_criteria(bakery)

                if start_condition_respected:
                    count_started += 1
                else:
                    count_start_condition_not_respected += 1

                LocalisationActionCollectionStatus.objects.update_or_create(
                    action_collection=action_collection,
                    localisation=bakery,
                    defaults={
                        'action_collection': action_collection,
                        'localisation': bakery,
                        'status': status_started if start_condition_respected else status_start_condition_not_respected
                    }
                )
                bar()

        color_print(f'SUCCESS: The action collection "{action_collection}" was started in {count_started} bakeries. {count_start_condition_not_respected} dit not meet the start condition (already vegan).', color='green')

        