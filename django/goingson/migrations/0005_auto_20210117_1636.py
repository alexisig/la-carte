# Generated by Django 3.1.5 on 2021-01-17 16:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('goingson', '0004_auto_20210117_1508'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='action',
            name='localisation',
        ),
        migrations.AddField(
            model_name='action',
            name='LocalisationType',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='goingson.localisationtype'),
            preserve_default=False,
        ),
    ]
