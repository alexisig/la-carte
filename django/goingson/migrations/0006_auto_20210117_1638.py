# Generated by Django 3.1.5 on 2021-01-17 16:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('goingson', '0005_auto_20210117_1636'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='action',
            name='LocalisationType',
        ),
        migrations.AddField(
            model_name='action',
            name='localisation',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='goingson.localisation'),
            preserve_default=False,
        ),
    ]
