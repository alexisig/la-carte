# Generated by Django 3.1.5 on 2021-01-19 16:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goingson', '0016_auto_20210118_1248'),
    ]

    operations = [
        migrations.AddField(
            model_name='action',
            name='objective',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
