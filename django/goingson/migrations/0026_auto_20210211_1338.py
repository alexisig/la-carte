# Generated by Django 3.1.5 on 2021-02-11 13:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('goingson', '0025_auto_20210211_1138'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actiondone',
            name='comment',
            field=models.TextField(blank=True, null=True, verbose_name='commentaire'),
        ),
        migrations.AlterField(
            model_name='actiondone',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='utilisateur'),
        ),
        migrations.AlterField(
            model_name='localisation',
            name='tags',
            field=models.JSONField(default=dict, help_text="\n            <p>Ce champ est renseigné dans le cas où une localisation est ajoutée depuis OpenStreetMap. <br>\n            Ne pas modifier manuellement: plutôt modifier le lieu sur OpenStreetMap directement (voir lien ci-dessous).\n            <p>Plus d'information à propos des données OpenStreetMap:\n            <p>\n                <a href='https://wiki.openstreetmap.org/wiki/Tags'>https://wiki.openstreetmap.org/wiki/Tags</a> <br>\n                <a href='https://taginfo.openstreetmap.org/'>https://taginfo.openstreetmap.org/</a>\n            </p>\n        ", verbose_name='Données OSM'),
        ),
        migrations.AlterField(
            model_name='localisationactioncollectionstatus',
            name='action_collection',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='goingson.actioncollection', verbose_name="collection d'action"),
        ),
        migrations.AlterField(
            model_name='localisationtype',
            name='display_name',
            field=models.CharField(default='', help_text='Ce nom apparaitra sur le site', max_length=255, verbose_name='Nom à afficher'),
        ),
    ]
