# Generated by Django 3.1.5 on 2021-02-24 10:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goingson', '0027_actiondone_opportunity'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='action',
            options={'ordering': ('collection', 'order_in_collection')},
        ),
        migrations.AlterField(
            model_name='action',
            name='order_in_collection',
            field=models.IntegerField(db_index=True, help_text='\n            <p>Les valeurs plus petites apparaissent en premières\n        ', verbose_name='Ordre dans la collection'),
        ),
    ]
