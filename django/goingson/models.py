from django.contrib.auth.models import User, Group
from django.db import models
from django.contrib.gis.db.models import PointField
from django.utils.timezone import utc
from Sonar.settings import REACT_BASE_URL
from Sonar.constants import MEDUSE_ROLE_NAME
from Sonar.permissions import is_in_group

import datetime
import os

class LocalisationActionCollectionStatus(models.Model):
    ''' Contains the status of an ActionCollection for a specific localisation. '''

    action_collection = models.ForeignKey(
        'ActionCollection',
        on_delete=models.CASCADE,
        verbose_name="collection d'action"
    )
    localisation = models.ForeignKey('Localisation', on_delete=models.CASCADE)
    status = models.ForeignKey('ActionCollectionStatus', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.action_collection.name} - {self.localisation.name} - {self.status.name}'

    def get_absolute_url(self):
        return f'{REACT_BASE_URL}/localisations/{self.localisation.id}'

    @property
    def current_action(self):
        actions = Action.objects \
            .filter(collection=self.action_collection) \
            .order_by('order_in_collection')

        current_action = None

        for action in actions:
            required_completions = action.required_completion_to_end

            completions = ActionDone.objects \
                .filter(action=action, localisation=self.localisation) \
                .count()
            
            if completions < required_completions:
                current_action = action
                break
        
        return current_action
    
    current_action.fget.short_description = 'Action en cours'

    @property
    def action_done(self):
        return ActionDone.objects.filter(
            localisation=self.localisation
        )

    class Meta:
        unique_together = ('action_collection', 'localisation',)
        verbose_name = "Status de collection d'action (par localisation)"
        verbose_name_plural = "Status de collection d'action (par localisation)"

class LocalisationType(models.Model):
    ''' Type of localisation. Exemple: bakery, book store etc. '''

    name = models.CharField(max_length=255, verbose_name='nom')
    display_name = models.CharField(
        max_length=255,
        default='',
        verbose_name='Nom à afficher',
        help_text='Ce nom apparaitra sur le site'
    )
    description = models.TextField()

    def __str__(self):
        return f'{self.display_name or self.name} ({Localisation.objects.count()})'

    class Meta:
        verbose_name = 'Type de localisation'

class Localisation(models.Model):
    ''' Single geographic entity. '''

    type = models.ForeignKey('LocalisationType', on_delete=models.CASCADE)
    osm_id = models.BigIntegerField(
        null=True,
        blank=True,
        verbose_name='ID OSM',
        help_text='''
            <p>Ce champ est renseigné dans le cas où une localisation est ajoutée depuis OpenStreetMap. <br>
            Ne pas modifier manuellement.
        '''
    )
    tags = models.JSONField(
        verbose_name='Données OSM',
        default=dict,
        help_text='''
            <p>Ce champ est renseigné dans le cas où une localisation est ajoutée depuis OpenStreetMap. <br>
            Ne pas modifier manuellement: plutôt modifier le lieu sur OpenStreetMap directement (voir lien ci-dessous).
            <p>Plus d'information à propos des données OpenStreetMap:
            <p>
                <a href='https://wiki.openstreetmap.org/wiki/Tags'>https://wiki.openstreetmap.org/wiki/Tags</a> <br>
                <a href='https://taginfo.openstreetmap.org/'>https://taginfo.openstreetmap.org/</a>
            </p>
        '''
    )
    '''

    OSM data is organised by tags, there is no set list of fields one can expect
    from a specific entity.

    Read more about OpenStreetMap tags here : https://wiki.openstreetmap.org/wiki/Tags
    List of available tags : https://taginfo.openstreetmap.org/

    '''
    geom = PointField(
        srid=4326,
        verbose_name='Position'
    )

    def __str__(self):
        return f'{self.id} - {self.type.display_name} - {self.name}'
    
    def get_absolute_url(self):
        return f'{REACT_BASE_URL}/localisations/{self.id}'

    @property
    def osm_url(self):
        return f'https://www.openstreetmap.org/node/{self.osm_id}'

    @property
    def name(self):
        return self.tags['name'] if 'name' in self.tags else self.osm_id

    @property
    def action_collections(self):
        return ActionCollection.objects.filter(localisation_type=self.type)

    @property
    def action_collection_status(self):
        return LocalisationActionCollectionStatus.objects.filter(localisation=self)

    @property
    def actions_available(self):
        # TODO: find a way to do it without a lot of requests
        return True

    def is_vegan(self):
        return self.tags['diet:vegan'] == 'only' if 'diet:vegan' in self.tags else False

    def is_vegan_friendly(self):
        return self.tags['diet:vegan'] == 'yes' if 'diet:vegan' in self.tags else False

class ActionCollectionStatus(models.Model):
    ''' Different type of statuses a ActionCollection can have. '''

    name = models.TextField(choices=(
        ('started', 'Started'),
        ('completed', 'Completed'),
        ('start_condition_not_respected', 'Not respecting start condition')
    ))
    description = models.TextField()

    def __str__(self):
        return self.name

class ActionDone(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    comment = models.TextField(
        null=True,
        blank=True,
        verbose_name='commentaire'
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name='utilisateur'
    )
    action = models.ForeignKey('Action', on_delete=models.CASCADE)
    localisation = models.ForeignKey('Localisation', on_delete=models.CASCADE)

    opportunity = models.BooleanField(null=True, blank=True)

    def __str__(self):
        return f'{self.action.collection.name} - {self.action.name} - {self.localisation.name} - {self.user}'

    @property
    def completions(self):
        return ActionDone.objects.filter(
            action=self.action,
            localisation=self.localisation,
            user=self.user
        )

    @property
    def time_since_last_completion(self):
        completions = self.completions

        if not completions:
            return None
        
        latest_completion = completions.latest('date')

        now = datetime.datetime \
            .utcnow() \
            .replace(tzinfo=utc)
        
        time_since_last_completion = now - latest_completion.date
        in_minutes = time_since_last_completion.total_seconds() / 60

        return in_minutes


    @property
    def time_until_next_possible_completion(self):
        time_since_last_completion = self.time_since_last_completion

        if time_since_last_completion is None:
            return 0
        
        diff = self.action.delay_before_repeat - time_since_last_completion

        return 0 if diff < 0 else diff

    @property
    def repeatable_now(self):
        if self.action.delay_before_repeat == 0:
            return True

        time_since_last_completion = self.time_since_last_completion

        if not time_since_last_completion:
            return True

        return time_since_last_completion >= self.action.delay_before_repeat

    class Meta:
        verbose_name = "Validations d'action"

class Action(models.Model):
    ''' Specific action to do in a specific place type (e.g. "ask for vegan croissant") '''

    name = models.CharField(
        max_length=255,
        verbose_name='nom',
        help_text='''
            <p>Nom court et catchy de l'action qui apparaitre sur le site.
            <p><i>Exemple: Opération croissant</i>
        '''
    )
    description = models.TextField(
        help_text='''
            <p>En quoi consiste la réalisation de l'action.
            <p><i>Exemple: demander un croissant vegan</i>
        '''
    )
    objective = models.TextField(
        verbose_name='objectif',
        help_text='''
            <p>Ce que va permettre l'action. Différent de l'objectif de la collection d'ation. 
            <p><i>Exemple: montrer à l'artisan boulangè.re une demande pour des produits vegan</i>
        '''
    )
    start_condition = models.TextField(
        verbose_name='Condition de départ',
        help_text='''
            <p>La ou les conditions pour que l'action soit effectuée dans un lieu
            <p><i>Exemple: le lieu ne doit pas déjà proposer des options vegans</i>
        '''
    )
    required_completion_to_end = models.IntegerField(
        verbose_name="Nom de fois que l'action doit être effectuée",
        help_text='''
            <p>Ce champ affichera un compteur sur le site <br>
            Une fois ce nombre atteint, la collection d'action à laquelle
            appartient l'action changera automatiquement d'action.
        '''
    )
    delay_before_repeat = models.IntegerField(
        default=0,
        verbose_name="Délai avant répétition",
        help_text='''
            <p>Ce champ limite à partir de combien de temps une action peut être répétée par un utilisateur dans une localisation
        '''
    )

    publication_date = models.DateTimeField(
        verbose_name='Date de publication',
        auto_now_add=True
    )

    collection = models.ForeignKey(
        'ActionCollection',
        on_delete=models.CASCADE,
        verbose_name="Collection d'action",
        help_text='''
            <p>Les actions ne sont pas assignées directement aux localisations. <br>
            Elles appartiennent à des collections, qui elles sont assignées aux localisations. <br>
            Cela permet d'imbriquer plusieurs actions dans une ordre prédéfini
        '''
    )
    order_in_collection = models.IntegerField(
        db_index=True,
        verbose_name="Ordre dans la collection",
        help_text='''
            <p>Les valeurs plus petites apparaissent en premières
        '''
    )

    def get_default_permission_group():
        group = Group.objects.get(name=MEDUSE_ROLE_NAME)
        if group:
            return [group]
        return []

    permission_to_complete = models.ManyToManyField(
        Group,
        default=get_default_permission_group,
        blank=True,
        verbose_name="Groupes autorisés à valider cette action",
        help_text='''
            <p>Seuls les groupes de la colonne de droite seronts autorisés à valider cette action
            <br />
        '''
    )

    def check_user_has_permission_to_complete(self, user):
        allowed_groups = self.permission_to_complete.all()

        for group in allowed_groups:
            if is_in_group(user, group):
                return True
        
        return False

    @property
    def next_action(self):
        return Action.objects.get(
            collection=self.collection,
            order_in_collection=self.order_in_collection + 1
        )

    def __str__(self):
        return f'{self.collection.name} - ({self.order_in_collection}) {self.name}'

    class Meta:
        unique_together = ('collection', 'order_in_collection',)
        ordering = ('collection', 'order_in_collection', )

class ActionCollection(models.Model):
    ''' Collection of actions. '''
    name = models.CharField(
        max_length=255,
        verbose_name='nom'
    )
    localisation_type = models.ForeignKey(
        'LocalisationType',
        on_delete=models.CASCADE,
        verbose_name='type de localisation'
    )

    @property
    def actions(self):
        return Action.objects.filter(collection=self)

    @property
    def actions_admin_str(self):
        return '\n'.join(map(lambda action: str(action), self.actions))

    actions_admin_str.fget.short_description = 'Actions'

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Collection d'action"
        