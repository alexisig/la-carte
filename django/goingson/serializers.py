from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from goingson.models import Localisation, LocalisationType, Action, ActionDone, ActionCollection, ActionCollectionStatus, LocalisationActionCollectionStatus
from discord_users.models import UserAvatar
from .exceptions import ActionNotRepeatable
from Sonar.constants import CAMPAIGNER_ROLE_NAME

class LocalisationTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LocalisationType
        fields = '__all__'

class ActionCollectionStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActionCollectionStatus
        fields = '__all__'

class LocalisationSerializer(GeoFeatureModelSerializer):
    type = serializers.SlugRelatedField(slug_field='name', read_only=True)
    actions_available = serializers.BooleanField(read_only=True)
    class Meta:
        model = Localisation
        fields = '__all__'
        geo_field = 'geom'

class UserSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField(read_only=True)
    groups = serializers.SlugRelatedField(slug_field='name', read_only=True, many=True)
    is_campaigner = serializers.SerializerMethodField()

    def get_avatar(self, obj):
        avatar = UserAvatar.objects.filter(user=obj).first()
        return avatar.url if avatar else None

    def get_is_campaigner(self, obj):
        return obj.groups.filter(name=CAMPAIGNER_ROLE_NAME).exists()

    class Meta:
        model = User
        fields = ('id', 'username', 'avatar', 'groups', 'is_campaigner')

class ActionDoneSerializer(serializers.ModelSerializer):
    action = serializers.PrimaryKeyRelatedField(queryset=Action.objects.all())
    localisation = serializers.PrimaryKeyRelatedField(queryset=Localisation.objects.all())
    user = UserSerializer(read_only=True)

    class Meta:
        model = ActionDone
        read_only_fields = ('date', 'user')
        fields = (
            'id',
            'date',
            'comment',
            'action',
            'localisation',
            'user',
            'repeatable_now',
            'time_until_next_possible_completion',
            'opportunity'
        )

    def validate(self, data):
        previous_actions = ActionDone.objects.filter(
            localisation=data['localisation'],
            action=data['action'],
            user=self.context['request'].user
        )

        if previous_actions:
            previous_action = previous_actions.first()
            if not previous_action.repeatable_now:
                raise ActionNotRepeatable()
        return data

    def create(self, validated_data):
        return ActionDone.objects.create(
            **validated_data,
            user=self.context['request'].user
        )

class ActionSerializer(serializers.ModelSerializer):
    user_has_permission_to_complete = serializers.SerializerMethodField(read_only=True)
    permission_to_complete = serializers.SlugRelatedField(slug_field='name', read_only=True, many=True)

    def get_user_has_permission_to_complete(self, obj: Action):
        return obj.check_user_has_permission_to_complete(self.context['request'].user)
    
    class Meta:
        model = Action
        fields = '__all__'

class UserActionDoneSerializer(ActionDoneSerializer):
    action = ActionSerializer()
    localisation = LocalisationSerializer()

class ActionCollectionSerializer(serializers.ModelSerializer):
    localisation_type = serializers.SlugRelatedField(slug_field='name', read_only=True)
    actions = ActionSerializer(many=True, read_only=True)
    class Meta:
        model = ActionCollection
        fields = '__all__'

class LocalisationActionCollectionStatusSerializer(serializers.ModelSerializer):
    current_action = serializers.PrimaryKeyRelatedField(read_only=True)
    status = serializers.SlugRelatedField(slug_field='name', read_only=True)
    action_collection = serializers.PrimaryKeyRelatedField(read_only=True)
    action_done = ActionDoneSerializer(many=True)

    class Meta:
        model = LocalisationActionCollectionStatus
        fields = (
            'current_action',
            'status',
            'action_collection',
            'action_done'
        )

class SingleLocalisationSerializer(GeoFeatureModelSerializer):
    type = serializers.SlugRelatedField(slug_field='name', read_only=True)
    action_collections = ActionCollectionSerializer(many=True, read_only=True)
    action_collection_status = LocalisationActionCollectionStatusSerializer(many=True, read_only=True)

    class Meta:
        model = Localisation
        fields = '__all__'
        geo_field = 'geom'
