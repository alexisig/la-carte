from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from django.contrib.auth.models import User, Group
from Sonar.constants import MEDUSE_ROLE_NAME
from django.core.cache import cache

DEFAULT_USERNAME = 'TEST_USER'
DEFAULT_PASSWORD = 'PASSWORD'

class APITestCaseWithAuth(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = self.create_test_user()
        self.meduse_group = Group.objects.create(name=MEDUSE_ROLE_NAME)
    
    def tearDown(self):
        cache.delete('7/64/44')

    def get_urls(self):
        return {
            'get': [
                '/api/localisations/1',
                '/api/localisations?tile=7/64/44',
                '/api/localisations_types/1/',
                '/api/localisations_types/',
                '/api/action_dones/',
                '/api/action_dones/1/'
            ]
        }

    def access_all_endpoints(self, authenticated, has_permissions):
        for request_method in self.get_urls():
            urls = self.get_urls()[request_method]
            for url in urls:
                response = self.client.get(url)

                if authenticated:
                    self.assertNotEqual(response.status_code, 401, msg=f'Endpoint {url} cannot be accessed with authentication.')
                    
                    if has_permissions:
                        self.assertNotEqual(response.status_code, 403, msg=f'Endpoint {url} cannot be accessed with permissions.')
                    else:
                        self.assertEqual(response.status_code, 403, msg=f'Endpoint {url} can be accessed without permissions.')
                
                else:
                    self.assertEqual(response.status_code, 401, msg=f'Endpoint {url} can be accessed without authentication.')
    
    def create_meduse_group(self):
        group = Group.objects.create(name=MEDUSE_ROLE_NAME)
        return group

    def update_user(self, opts={}):
        User.objects.filter(pk=self.user.pk).update(**opts)

    def create_test_user(self):
        user = User(
            username=DEFAULT_USERNAME,
            is_active=True
        )
        user.set_password(DEFAULT_PASSWORD)
        user.save()
        return user

    def authenticate(self, username=DEFAULT_USERNAME, password=DEFAULT_PASSWORD):
        user_data = {
            'username': username,
            'password': password
        }
        response = self.client.post('/auth/token/obtain', data=user_data, format='json')
        as_json = response.json()
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + as_json['token'])
