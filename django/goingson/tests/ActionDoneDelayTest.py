from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from django.contrib.auth.models import User
from goingson.models import Action, Localisation, LocalisationType, ActionCollectionStatus, LocalisationActionCollectionStatus, ActionCollection, ActionDone
from django.conf import settings
from django.urls import URLPattern, URLResolver
from .APITestCaseWithAuth import APITestCaseWithAuth

from django.utils import timezone

class ActionDoneDelayTest(APITestCaseWithAuth):
    fixtures = [
        'localisationtype.json',
        'action.json',
        'actioncollection.json',
        'actioncollectionstatus.json',
        'localisationtype.json'
    ]

    def test(self):
        self.user.groups.add(self.meduse_group)
        self.authenticate()

        localisation_type = LocalisationType.objects.get(name='bakery')

        collection = ActionCollection.objects.create(
            name='Collection de test',
            localisation_type=localisation_type
        )

        action = Action.objects.create(
            name='Action de test 1',
            collection=collection,
            required_completion_to_end=10,
            description=' ',
            delay_before_repeat=10,
            objective=' ',
            start_condition=' ',
            publication_date=timezone.now(),
            order_in_collection=1
        )

        action.permission_to_complete.set([self.meduse_group])

        bakery = Localisation.objects.create(
            type=localisation_type,
            osm_id=100,
            tags={},
            geom='POINT(0 0)'
        )

        completions = ActionDone.objects.filter(
            localisation=bakery,
            action=action,
            user=self.user
        )

        response = self.client.post('/api/action_dones/', data={
            "comment": "Propriétaires très sympas !",
            "action": action.pk,
            "localisation": bakery.pk
        })

        completions = ActionDone.objects.filter(
            localisation=bakery,
            action=action,
            user=self.user
        )
        first_completion = completions.first()

        self.assertFalse(first_completion.repeatable_now, msg='Action can be repeated before the delay.')


