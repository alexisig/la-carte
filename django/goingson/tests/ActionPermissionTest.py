from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from django.contrib.auth.models import User, Group
from goingson.models import Action, Localisation, LocalisationType, ActionCollectionStatus, LocalisationActionCollectionStatus, ActionCollection
from django.conf import settings
from django.urls import URLPattern, URLResolver
from .APITestCaseWithAuth import APITestCaseWithAuth
from Sonar.constants import MEDUSE_ROLE_NAME

from django.utils import timezone

class ActionPermissionTest(APITestCaseWithAuth):
    fixtures = [
        'localisationtype.json',
        'action.json',
        'actioncollection.json',
        'actioncollectionstatus.json',
        'localisationtype.json'
    ]

    def setUp(self):
        super(ActionPermissionTest, self).setUp()

        self.localisation_type = LocalisationType.objects.first()
        self.localisation = Localisation.objects.create(
            type=self.localisation_type,
            osm_id=100,
            tags={},
            geom='POINT(0 0)'
        )
        self.collection = ActionCollection.objects.create(
            name='Collection de test',
            localisation_type=self.localisation_type
        )
        self.action = Action.objects.create(
            name='Action de test',
            collection=self.collection,
            required_completion_to_end=10,
            description=' ',
            delay_before_repeat=0,
            objective=' ',
            start_condition=' ',
            publication_date=timezone.now(),
            order_in_collection=1
        )

        self.action.permission_to_complete.set([self.meduse_group])

    def test_user_not_in_action_group_permission_cannot_complete_action(self):
        self.authenticate()

        response = self.client.post('/api/action_dones/', data={
            "comment": "Propriétaires très sympas !",
            "action": self.action.pk,
            "localisation": self.localisation.pk
        })

        self.assertEqual(response.status_code, 403, msg=f'User not in action group permission can complete action')

    def test_user_in_action_group_permission_can_complete_action(self):
        self.authenticate()
        self.user.groups.add(self.meduse_group)

        response = self.client.post('/api/action_dones/', data={
            "comment": "Propriétaires très sympas !",
            "action": self.action.pk,
            "localisation": self.localisation.pk
        })

        self.assertEqual(response.status_code, 201, msg=f'User in action group permission cannot complete action')  