from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from django.contrib.auth.models import User, Group
from goingson.models import Action, Localisation, LocalisationType, ActionCollectionStatus, LocalisationActionCollectionStatus, ActionCollection
from django.conf import settings
from django.urls import URLPattern, URLResolver
from .APITestCaseWithAuth import APITestCaseWithAuth
from Sonar.constants import MEDUSE_ROLE_NAME

from django.utils import timezone

class CurrentActionStatusTest(APITestCaseWithAuth):
    fixtures = [
        'localisationtype.json',
        'action.json',
        'actioncollection.json',
        'actioncollectionstatus.json',
        'localisationtype.json'
    ]

    def test(self):
        self.user.groups.add(self.meduse_group)
        self.authenticate()

        localisation_type = LocalisationType.objects.get(name='bakery')

        collection = ActionCollection.objects.create(
            name='Collection de test',
            localisation_type=localisation_type
        )

        action_1 = Action.objects.create(
            name='Action de test 1',
            collection=collection,
            required_completion_to_end=10,
            description=' ',
            delay_before_repeat=0,
            objective=' ',
            start_condition=' ',
            publication_date=timezone.now(),
            order_in_collection=1
        )

        action_1.permission_to_complete.set([self.meduse_group])

        action_2 = Action.objects.create(
            name='Action de test 2',
            collection=collection,
            required_completion_to_end=10,
            description=' ',
            delay_before_repeat=0,
            objective=' ',
            start_condition=' ',
            publication_date=timezone.now(),
            order_in_collection=2
        )

        action_2.permission_to_complete.set([self.meduse_group])

        bakery = Localisation.objects.create(
            type=localisation_type,
            osm_id=100,
            tags={},
            geom='POINT(0 0)'
        )
        started_status = ActionCollectionStatus.objects.get(name='started')
        status = LocalisationActionCollectionStatus.objects.create(
            action_collection=collection,
            localisation=bakery,
            status=started_status
        )

        self.assertEqual(status.current_action, action_1, msg='Wrong initial current action')

        for _ in range(action_1.required_completion_to_end):
            response = self.client.post('/api/action_dones/', data={
                "comment": "Propriétaires très sympas !",
                "action": action_1.pk,
                "localisation": bakery.pk
            })

        message = 'ActionCollectionStatus does not update current_action automatically when required_completion is met.'
        self.assertNotEqual(status.current_action, action_1, msg=message)
        self.assertEqual(status.current_action, action_2, msg=message)