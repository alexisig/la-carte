from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from django.contrib.auth.models import User, Group
from django.conf import settings
from django.urls import URLPattern, URLResolver
from .APITestCaseWithAuth import APITestCaseWithAuth

class NonStaffNonMeduseCannotAccessEndpoints(APITestCaseWithAuth):
    def test(self):
        self.access_all_endpoints(authenticated=False, has_permissions=False)
        self.authenticate()
        self.access_all_endpoints(authenticated=True, has_permissions=False)

class StaffNonMeduseCanAccessEndpoints(APITestCaseWithAuth):
    def test(self):
        self.update_user({ 'is_staff': True })
        self.access_all_endpoints(authenticated=False, has_permissions=False)
        self.authenticate()
        self.access_all_endpoints(authenticated=True, has_permissions=True)

class MeduseNonStaffCanAccessEndpoints(APITestCaseWithAuth):
    def test(self):
        self.user.groups.add(self.meduse_group)
        self.access_all_endpoints(authenticated=False, has_permissions=False)
        self.authenticate()
        self.access_all_endpoints(authenticated=True, has_permissions=True)