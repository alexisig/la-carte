from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from django.contrib.auth.models import User
from django.conf import settings
from django.urls import URLPattern, URLResolver

class JWTAuthenticationTest(APITestCase):
    fixtures = [
        'localisationtype.json',
    ]

    def setUp(self): 
        self.user = User(
            email='alexis.athlani@gmail.com',
            username='JWTAuthenticationTestUser',
            is_staff=True,
            is_active=True
        )
        self.user.set_password('J9_B%}G$=q=<zYeP')
        self.user.save()

    def test_create_account(self):
        """
        Test user creation
        """
        first_user = User.objects.first()
        self.assertEqual(first_user.username, 'JWTAuthenticationTestUser', 'User creation failed.')

    def test_jwt_auth(self):
        # Obtain token
        user_data = {
            'username': 'JWTAuthenticationTestUser',
            'password': 'J9_B%}G$=q=<zYeP'
        }
        response = self.client.post('/auth/token/obtain', data=user_data, format='json')
        as_json = response.json()
        response_contains_token = 'token' in as_json
        token = as_json['token'] or ''
        self.assertTrue(response_contains_token, msg='JWT token obtention failed.')

        # Verify correct token
        data_token = { 'token': token }
        response = self.client.post('/auth/token/verify', data=data_token, format='json')
        self.assertEqual(response.status_code, 200, msg='Correct JWT token verify failed.')

        # Verify wrong token
        wrong_data_token = { 'token': 'wrong_token' }
        response = self.client.post('/auth/token/verify', data=wrong_data_token, format='json')
        self.assertEqual(response.status_code, 400, msg='Wrong JWT token verify failed.')

        # Refresh token
        response = self.client.post('/auth/token/refresh', data=data_token, format='json')
        as_json = response.json()
        response_contains_token = 'token' in as_json
        self.assertTrue(response_contains_token, msg='JWT token refresh failed.')

        # Access endpoint without token
        response = self.client.get('/api/localisations_types/1/')
        self.assertEqual(response.status_code, 401, msg='User can access data without token.')

        # Access endpoint with wrong token
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + 'wrong_token')
        response = self.client.get('/api/localisations_types/1/')
        self.assertEqual(response.status_code, 401, msg='User can access data with a wrong token.')

        # Access data with correct token
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)
        response = self.client.get('/api/localisations_types/1/')
        self.assertEqual(response.status_code, 200, msg='User cannot access data with correct token.')