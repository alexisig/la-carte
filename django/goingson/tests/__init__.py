from .EndpointAuthenticationTest import NonStaffNonMeduseCannotAccessEndpoints, \
    StaffNonMeduseCanAccessEndpoints, \
    MeduseNonStaffCanAccessEndpoints
from .JWTAuthenticationTest import JWTAuthenticationTest
from .CurrentActionStatusTest import CurrentActionStatusTest
from .ActionDoneDelayTest import ActionDoneDelayTest
from .ActionPermissionTest import ActionPermissionTest