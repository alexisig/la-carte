from django.urls import path
from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register(r'localisations_types', views.LocalisationTypeViewset)
router.register(r'action_dones', views.ActionDoneViewset)
router.register(r'action_collections', views.ActionCollectionViewset)

urlpatterns = [
    path('localisations', views.LocalisationList().as_view()),
    path('localisations/<int:pk>', views.SingleLocalisation().as_view()),
    path('user/action_dones', views.UserActionDoneViewset().as_view()),
] + router.urls
