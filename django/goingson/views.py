from django.http import Http404
from django.http import HttpResponse

from django.shortcuts import render, get_object_or_404
from django.template import loader
from goingson.serializers import LocalisationSerializer, SingleLocalisationSerializer, LocalisationTypeSerializer, ActionDoneSerializer, ActionCollectionSerializer, UserActionDoneSerializer
from rest_framework_gis.filters import InBBoxFilter, TMSTileFilter
from rest_framework_gis.pagination import GeoJsonPagination
from rest_framework.pagination import PageNumberPagination
from rest_framework import generics, viewsets, response
from rest_framework import renderers
from .models import Action, Localisation, LocalisationType, ActionDone, ActionCollection
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_control
from django.core.cache import cache

SIX_HOURS = 60 * 60 * 6

localisation_list_decorators = [cache_control(max_age=SIX_HOURS)]

@method_decorator(localisation_list_decorators, name='get')
class LocalisationList(generics.ListAPIView):
    queryset = Localisation.objects.order_by('osm_id').all().prefetch_related('type')
    bbox_filter_field = 'geom'
    filter_backends = (InBBoxFilter, TMSTileFilter, )

    def get_serializer_class(self):
        in_bbox = self.request.GET.get('in_bbox')
        if in_bbox:
            return SingleLocalisationSerializer
        return LocalisationSerializer

    def get(self, *args, **kwargs):
        in_bbox = self.request.GET.get('in_bbox')
        if in_bbox:
            response = super(LocalisationList, self).get(*args, **kwargs)
            
            response.accepted_renderer = self.request.accepted_renderer
            response.accepted_media_type = self.request.accepted_media_type
            response.renderer_context = self.get_renderer_context()

            return response.render()
        
        tile = self.request.GET.get('tile')

        cached = cache.get(tile)

        if cached:
            return cached
            
        response = super(LocalisationList, self).get(*args, **kwargs)
        response.accepted_renderer = self.request.accepted_renderer
        response.accepted_media_type = self.request.accepted_media_type
        response.renderer_context = self.get_renderer_context()

        rendered_reponse = response.render()

        cache.set(tile, rendered_reponse, SIX_HOURS)

        return cache.get(tile)

class SingleLocalisation(generics.RetrieveAPIView):
    queryset = Localisation.objects.all()
    serializer_class = SingleLocalisationSerializer
    lookup_field = 'pk'

class LocalisationTypeViewset(viewsets.ReadOnlyModelViewSet):
    queryset = LocalisationType.objects.all()
    serializer_class = LocalisationTypeSerializer

class ActionCollectionViewset(viewsets.ReadOnlyModelViewSet):
    queryset = ActionCollection.objects.all()
    serializer_class = ActionCollectionSerializer

class ActionDoneViewset(viewsets.ModelViewSet):
    queryset = ActionDone.objects.all()
    serializer_class = ActionDoneSerializer

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        authorized = serializer \
            .validated_data \
            .get('action') \
            .check_user_has_permission_to_complete(request.user)
        
        if not authorized:
            return response.Response(data={
                'detail': 'Forbidden'
            }, status=403)
        
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return response.Response(serializer.data, status=201, headers=headers)     
       
class UserActionDonePagination(PageNumberPagination):
    page_size = 10
    
class UserActionDoneViewset(generics.ListAPIView):
    pagination_class = UserActionDonePagination
    serializer_class = UserActionDoneSerializer

    def get_queryset(self):
        user = self.request.user
        return ActionDone.objects.filter(user=user)
