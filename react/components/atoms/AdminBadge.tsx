import React from 'react'
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import Typography from '@material-ui/core/Typography';
import { Box } from '@material-ui/core';

export const AdminBadge = () => (
    <Box display="flex" flexDirection="row" alignItems="center">
        <Box mr={1}><AssignmentIndIcon /></Box>
        <Typography variant="caption">Chargé•e de campagne</Typography>
    </Box>
)
