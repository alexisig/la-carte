import Link from 'next/link'
import styled from 'styled-components'
import { connect } from 'react-redux'
import User from '../../store/models/User'
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Home';
import MapIcon from '@material-ui/icons/Map';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import InfoIcon from '@material-ui/icons/Info';
import AddBoxIcon from '@material-ui/icons/AddBox';
import { PROJET_MEDUSES } from '../styles/Colors';

interface FooterProps {
    className?: string;
    user: User
}

interface LinkWrapperProps {
    label?: string
    href: string;
    show: boolean;
    children?: any
}

const LinkWrapper = ({ label, href, show, children } : LinkWrapperProps) => show && (
    <Link href={href}>
        <Button>{label || children}</Button>
    </Link>
)

const Footer = ({ className, user }: FooterProps) => {
    const userAuthenticated = user.isAuthenticated()
    return (
        <footer className={className}>
            <AppBar position='fixed'>
                <LinkWrapper show href='/'>
                        <HomeIcon />
                    </LinkWrapper>
                    <LinkWrapper show={userAuthenticated} href='/map'>
                        <MapIcon />
                    </LinkWrapper>
                    <LinkWrapper show href='/about'>
                        <InfoIcon />
                    </LinkWrapper>
                    <LinkWrapper show={!userAuthenticated} href='/login-django'>
                        <ExitToAppIcon />
                    </LinkWrapper>
                    <LinkWrapper show={!userAuthenticated} href='/signup'>
                        <AddBoxIcon />
                    </LinkWrapper>
                    <LinkWrapper show={userAuthenticated} href='/logout'>
                        <ExitToAppIcon />
                    </LinkWrapper>  
            </AppBar>
        </footer>
    )
}

export default connect((store) => ({
    user: store.get('user'),
}))(styled(Footer)`
    .MuiAppBar-root {
        bottom: 0px;
        top: initial;
        background: ${PROJET_MEDUSES.DARK_GREEN};
    }
    header {
        display: flex;
        flex-direction: row;
        justify-content: space-evenly;
        button {
            flex-basis: 25%;
            color: #eee;
        }
    }
`)