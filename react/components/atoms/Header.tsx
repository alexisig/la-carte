import Link from 'next/link';
import styled from 'styled-components'
import { connect } from 'react-redux'
import User from '../../store/models/User'
import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';

interface HeaderProps {
    className?: string
    user: User
}

const LogoMeduse = styled.img`
    height: 1.5em;
`

const Header = ({ user, className }: HeaderProps) => {
    return (
        <header className={className}>
            <AppBar position="relative">
                <Toolbar>
                <IconButton edge="start" color="inherit" aria-label="menu">
                    <LogoMeduse src='https://hub.projet-meduses.com/apps/theming/image/logoheader?v=9' />
                </IconButton>
                <Typography variant="h5">La Carte</Typography>
                {user.isAuthenticated() && (
                    <Link href="/profile">
                        <IconButton edge="end" color="inherit" aria-label="menu">
                            <Avatar src={user.get('avatar')} alt={user.get('avatar')} />
                        </IconButton>
                    </Link>
                )}
                </Toolbar>
            </AppBar>
        </header>
    )
}

export default connect((store) => ({
    user: store.get('user')
}))(styled(Header)`
    .MuiAppBar-root {
        background: transparent;
    }
    .MuiTypography-root {
        flex-grow: 1;
    }

`)
