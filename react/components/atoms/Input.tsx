import * as React from 'react'
import styled from "styled-components"
import { GREY } from '../styles/Colors'


const Input = (props) => {
    return <input {...props} />
}

export default styled(Input)`
    outline: none;
    border: 1px solid ${GREY};
`