import React, { FC } from 'react'
import { useSelector } from 'react-redux'
import { List, Map } from 'immutable';
import Alert from '@material-ui/lab/Alert';

import Opportunity from '../../store/models/Opportunity';

interface Props {
    opportunity: boolean | null;
}

export const OpportunityCard: FC<Props> = ({ opportunity }) => {
    const opportunities: List<Opportunity> = useSelector((store: Map<string, any>) => store.get('opportunities'));
    const op = opportunities.find(op => op.value === opportunity)
    return <Alert severity={op.color}>{op.message}</Alert>;
}
