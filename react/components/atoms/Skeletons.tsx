import React from 'react'
import List from '@material-ui/core/List'
import Box from '@material-ui/core/Box'
import Skeleton from '@material-ui/lab/Skeleton'

export const SkeletonListItem = () => (
    <Box mb={1} boxShadow={1} borderRadius={4} overflow="hidden"><Skeleton variant="rect" height={48} /></Box>
)

interface SkeletonListProps {
    count: number;
}

export const SkeletonList = ({ count = 3 }: SkeletonListProps) => {
    return (
        <List>
            { Array(count).fill('').map((_, i) => (
                <SkeletonListItem />
            ))}
        </List>
    )
}
