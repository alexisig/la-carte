import dynamic from 'next/dynamic'
import { IMapContainerProps } from '../ecosystems/MapContainer'
import { LocalisationCardPropsInterface } from '../molecules/LocalisationCard'

export const MapContainerNoSSR = dynamic<IMapContainerProps>(
    () => import('../ecosystems/MapContainer'),
    { ssr: false }
)

export const ModalNoSSR = dynamic<{}>(
    () => import('../molecules/Modal'),
    { ssr: false }  
)

export const LocalisationCardNoSSR = dynamic<LocalisationCardPropsInterface>(
    () => import('../molecules/LocalisationCard'),
    { ssr: false }
)