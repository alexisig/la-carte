import React from 'react'
import styled from 'styled-components'
import map from '../map/map'
import { unByKey } from 'ol/Observable'
import { setClickedFeature, setHoveredFeature, setPosition } from '../../store/actions'
import { setResultsFromFeatures } from '../../store/actions-frontend'
import { connect } from "react-redux";
import getPopup from '../map/popup'
import LocalisationSmallCard from '../molecules/LocalisationSmallCard'
import { localisationsSource, localisationsSourceZoomedIn, NO_CACHE_AFTER_ZOOM } from '../map/layers/localisations'
import debounce from '../../utils/debounce'
import { createEmpty, extend, buffer } from 'ol/extent'
import view from '../map/view'
import { fromLonLat, toLonLat } from 'ol/proj'
import Position from '../../store/models/position'
import Geolocation from '../../store/models/Geolocation'
import { geolocation, onGeolocationChange } from '../map/controls/geolocation'
import { Router, withRouter } from 'next/router'
import { Overlay } from 'ol/Overlay'
import { Point } from 'ol/geom/Point'

const MapElement = styled.div`
    width: 100%;
    height: 100vh;
`

const PopupElement = styled.div``

const setCursorToPointer = () => {
  const mapElement = document.getElementById('map')
  mapElement.style.cursor = 'pointer'
}

const setCursorToNormal = () => {
  const mapElement = document.getElementById('map')
  mapElement.style.cursor = 'initial'
}
 
export interface IMapContainerProps {
  results?: any;
  hovered_feature?: any;
  initialPosition?: Position;
  position?: Position;
  geolocation?: Geolocation
  router?: Router;
}

export interface IMapContainerState {
  popup: Overlay;
  mapCenteredOnUserPosition: boolean;
  mapWasCenteredOnInitialPosition: boolean;
  localisationsCleared: boolean
}

class MapContainer extends React.Component<IMapContainerProps, IMapContainerState> {
  constructor(props: IMapContainerProps) {
    super(props)
    this.state = {
      popup: null,
      mapCenteredOnUserPosition: false,
      mapWasCenteredOnInitialPosition: false,
      localisationsCleared: false
    }
  }
  olEventsKeys = null

  localisationLayerFilter = (layer) => {
    const name = layer.get('name')
    return name && (
      name === 'localisations' ||
      name === 'localisations_zoomed_in'
    )
  }

  onMapPointerMove = (event) => {
    const { pixel } = event
    const { popup } = this.state

    const features = map.getFeaturesAtPixel(pixel, {
      layerFilter: this.localisationLayerFilter
    })
    const firstFeature = features.length > 0 ? features[0] : null

    if (firstFeature) {
      setCursorToPointer()
    } else {
      setHoveredFeature(null)
      popup.setPosition(null)
      setCursorToNormal()
      return
    }

    const isFirstFeatureACluster = !!firstFeature.get('features')
    const featureCountInCluster = isFirstFeatureACluster && firstFeature.get('features').length

    let feature = null 

    if (isFirstFeatureACluster && featureCountInCluster === 1) {
      feature = firstFeature.get('features')[0]
    } else {
      feature = firstFeature
    }

    const geometry = feature.getGeometry() as Point
    const coordinates = geometry.getCoordinates()
    popup.setPosition(coordinates)
    setHoveredFeature((feature as any).getId())
  }

  onMapSingleClick = (event) =>  {
    const { hovered_feature, router } = this.props
    const { pixel } = event
    const features = map.getFeaturesAtPixel(pixel, {
      layerFilter: this.localisationLayerFilter
    })
    const firstFeature = features.length > 0 ? features[0] : null
    if (!firstFeature) {
      return
    }

    const isFirstFeatureACluster = !!firstFeature.get('features')
    const featureCountInCluster = isFirstFeatureACluster && firstFeature.get('features').length

    if (isFirstFeatureACluster && featureCountInCluster > 1) {
        const extent = createEmpty();
        firstFeature
          .get('features')
          .forEach((f) => extend(extent, f.getGeometry().getExtent()))
        view.fit(buffer(extent, 100), {
          size: map.getSize(),
          duration: 400,
        })
        return
    }

    let feature = null

    if (isFirstFeatureACluster && featureCountInCluster === 1) {
      feature = firstFeature.get('features')[0]
    } else [
      feature = firstFeature
    ]

    const featureId = hovered_feature ? hovered_feature.id : feature.getId()
    const path = `/localisations/${featureId}`

    setClickedFeature(featureId)

    router.push(path, path, { shallow: true })
  }

  onLocalisationLayerUpdate = () => {
    const zoom = view.getZoom()
    const extent = view.calculateExtent(map.getSize())

    let features = []

    if (zoom > NO_CACHE_AFTER_ZOOM) {
      features = localisationsSourceZoomedIn.getFeaturesInExtent(extent)
    } else {
      features = localisationsSource.getFeaturesInExtent(extent)
    }

    setResultsFromFeatures(features)
  }

  onLocalisationLayerUpdateDebounced = debounce(this.onLocalisationLayerUpdate, 1000, null)

  updateUrlOnMoveEnd = () => {
    const { router } = this.props
    const [ lon, lat ] = toLonLat(view.getCenter())
      .reverse()
      .map(e => e.toFixed(4))

    const zoom = view.getZoom() 
    const path = `/map/${lon}/${lat}/${zoom.toPrecision(4)}`

    router.push('', path, { shallow: true })
    setPosition(new Position({ lat, lon, zoom }))
  }

  setMapEvents() {
    this.olEventsKeys = [
      map.on('pointermove', this.onMapPointerMove),
      map.on('singleclick', this.onMapSingleClick),
      localisationsSource.on('tileloadend', this.onLocalisationLayerUpdateDebounced),
      map.on('change:view', this.onLocalisationLayerUpdateDebounced),
      map.on('moveend', () => {
        this.updateUrlOnMoveEnd()
        this.onLocalisationLayerUpdateDebounced()
      }),
      geolocation.on('change', onGeolocationChange)
    ]
  }
  
  unsetMapEvents() {
    unByKey(this.olEventsKeys)
  }

  setInitialPosition() {
    const { initialPosition, position } = this.props

    if (!initialPosition && !position) {
      return
    }
    const pos = initialPosition || position
    const { lat, lon, zoom } = pos
    view.setCenter(fromLonLat([lat, lon]))
    view.setZoom(zoom)
    this.setState({ mapWasCenteredOnInitialPosition: true })
    this.updateUrlOnMoveEnd()
  }

  componentDidUpdate(prevProps) {
    const { geolocation, position } = this.props
    const {
      mapCenteredOnUserPosition,
      mapWasCenteredOnInitialPosition,
    } = this.state

    const shouldCenterMapOnUserPosition = (
      !mapWasCenteredOnInitialPosition &&
      !!geolocation.get('coordinates') &&
      !mapCenteredOnUserPosition
    )

    if (shouldCenterMapOnUserPosition) {
      view.setCenter(geolocation.get('coordinates'))
      view.setZoom(19)
      this.setState({ mapCenteredOnUserPosition: true })
    }
  }

  componentDidMount() {
    map.setTarget('map')
    const popup = getPopup({
      element: document.getElementById('popup')
    })
    map.addOverlay(popup)
    geolocation.setTracking(true)

    this.setMapEvents()
    this.setState({ popup })
    this.setInitialPosition()
    localisationsSourceZoomedIn.refresh()
  }

  componentWillUnmount() {
    const { popup } = this.state

    map.setTarget(null)
    popup.setElement(null)
    map.removeOverlay(popup)
    geolocation.setTracking(false)
    this.unsetMapEvents()
  }

  public render() {
    const { hovered_feature } = this.props 
    return (
      <>
        <MapElement id='map' />
        <PopupElement id='popup'>
            {hovered_feature && <LocalisationSmallCard localisation={hovered_feature} />}
        </PopupElement>
      </>
    );
  }
}

export default withRouter(connect((store) => ({
  results: store.get('results'),
  hovered_feature: store.getIn(['map', 'hovered_feature']),
  geolocation: store.get('geolocation'),
  position: store.get('position')
}))(MapContainer))
