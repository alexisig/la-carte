import styled from 'styled-components'
import Footer from '../atoms/Footer'
import Header from '../atoms/Header'
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { PROJET_MEDUSES, WHITE } from '../styles/Colors';

const LayoutWrapper = styled.div`
    height: 100vh;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
`

interface LayoutContentPropsInterface {
    noPadding: boolean;
    scrollable: boolean;
    gradientBackground: boolean;
}

const LayoutContent = styled.div<LayoutContentPropsInterface>`
    overflow: ${({ scrollable }) => scrollable ? 'scroll': 'hidden'};
    padding: ${({ noPadding }) => noPadding ? '0px' : '15px'};
    background-color: ${({ gradientBackground }) => gradientBackground ? `
        linear-gradient(
            135deg,
            ${PROJET_MEDUSES.RED} 5%,
            ${PROJET_MEDUSES.DARK_GREEN} 45%,
            ${PROJET_MEDUSES.DARK_GREEN} 55%,
            ${PROJET_MEDUSES.CYAN} 95%
        )
    `: `${WHITE}`};
    flex-grow: 1;
`

const Layout = ({ children }) => {
    const theme = useTheme()
    const IsMobile = useMediaQuery(theme.breakpoints.down('sm'))

    const { displayName } = children.type
    return (
        <LayoutWrapper>
            <Header />
            <LayoutContent
                scrollable={displayName !== 'map'}
                noPadding={displayName === 'map'}
                gradientBackground={displayName === 'map'}
                >
                {children}
                {IsMobile && <><br /><br /><br /></>}
            </LayoutContent>
            <Footer />
        </LayoutWrapper>
    )
}

export default Layout
