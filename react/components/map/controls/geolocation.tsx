import Geoloc from 'ol/Geolocation'
import { setInGeolocation } from '../../../store/actions'
import view from '../view'
import { geolocationFeature, accuracyFeature } from '../layers/geolocation'
import Point from 'ol/geom/Point'
import { Geolocation } from 'openlayers'

export const geolocation = new Geoloc({
    projection: view.getProjection(),
    trackingOptions: {
        enableHighAccuracy: true // needed for the heading value
    }
}) as Geolocation

export const onGeolocationChange = () => {
    const accuracy = geolocation.getAccuracy()
    const heading = geolocation.getHeading()
    const speed = geolocation.getSpeed()
    const coordinates = geolocation.getPosition()
    const accuracyGeometry = geolocation.getAccuracyGeometry()
    setInGeolocation({
      accuracy,
      accuracyGeometry,
      heading,
      speed,
      coordinates
    })
    geolocationFeature.setGeometry(new Point(coordinates))
    accuracyFeature.setGeometry(accuracyGeometry)
}