import { defaults as defaultControls } from "ol/control";

export const controls = defaultControls({
    zoom: true,
    attribution : false,
    rotate: false,
}).extend([
    // add other controls here
])