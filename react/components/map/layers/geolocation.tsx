import VectorLayer from 'ol/layer/Vector'
import VectorSource from 'ol/source/Vector'
import Feature from 'ol/Feature';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style';

export const geolocationFeature = new Feature() as ol.Feature
export const accuracyFeature = new Feature() as ol.Feature

export default new VectorLayer({
    style: new Style({
          image: new CircleStyle({
            radius: 6,
            fill: new Fill({
              color: '#3399CC',
            }),
            stroke: new Stroke({
              color: '#fff',
              width: 2,
            }),
          }),
    }),
    source: new VectorSource({
        features: [ geolocationFeature, accuracyFeature ]
    })
}) as ol.layer.Vector