import VectorSource from 'ol/source/Vector'
import VectorLayer from 'ol/layer/Vector'
import { createXYZ } from 'ol/tilegrid';
import GeoJSON from 'ol/format/GeoJSON'
import { tile, bbox } from 'ol/loadingstrategy'
import { getCenter } from 'ol/extent'
import localisationsStyle, { singleLocalisationStyle } from '../styles/localisationsStyle';
import Cluster from 'ol/source/Cluster';
import { getFetchRequest } from '../../../store/jwt';
import AnimatedCluster from 'ol-ext/layer/AnimatedCluster'
import { transformExtent } from 'ol/proj';

/**
 * 
 * Localisations are displayed through two layers:
 * 1) localisations
 * 2) localisationsZoomedIn
 * 
 * 1) localisations
 * 
 * localisations makes request to the server with tiles.
 * The tile request are limited to the zoom level 7 to increase 
 * the chance of multiple users hitting the same cached tile.
 * More info about this type of request:
 * https://github.com/openwisp/django-rest-framework-gis#tmstilefilter
 * 
 * 2) localisationsZoomedIn
 * 
 * localisationsZoomedIn makes requests to the server with bbox.
 * These requests are not cached and will show 'live data'.
 * More info about this type of request:
 * https://github.com/openwisp/django-rest-framework-gis#inbboxfilter
 * 
 */

export const NO_CACHE_AFTER_ZOOM = 17
export const MAIN_ZOOM = 7
export const CLUSTER_DISTANCE = 50
export const MAX_RESOLUTION = 50
const featureProjection = 'EPSG:3857'
const dataProjection = 'EPSG:4326'

const format = new GeoJSON({ featureProjection, dataProjection })
const tileGrid = createXYZ({ minZoom: MAIN_ZOOM, maxZoom: MAIN_ZOOM })

export const localisationSourceLoader = async function(extent : ol.Extent, resolution: number, clear=false) {
    const zoom = tileGrid.getZForResolution(resolution)

    const extentCenter = getCenter(extent)

    const [ z, x, y ] = tileGrid.getTileCoordForCoordAndZ(
        extentCenter,
        zoom
    )

    const url = `${process.env.NEXT_PUBLIC_DJANGO_BASE_URL}/api/localisations?tile=${z}/${x}/${y}${zoom > NO_CACHE_AFTER_ZOOM ? `&no_cache=${Math.random()}` : ''}`
    const response = await getFetchRequest(url)
    if (response.status !== 200) {
        return
    }
    const responseJson = await response.json()
    if (!responseJson) {
        return
    }

    const features = (localisationsSource
        .getFormat() as any)
        .readFeatures(responseJson)

    localisationsSource.addFeatures(features)

    return Promise.resolve()
}

export const localisationZoomedInSourceLoader = async function(extent: ol.Extent) {
    const projectedExtent = transformExtent(extent, featureProjection, dataProjection)
    const url = `${process.env.NEXT_PUBLIC_DJANGO_BASE_URL}/api/localisations?in_bbox=${projectedExtent.join(',')}`
    const response = await getFetchRequest(url)
    if (response.status !== 200) {
        return
    }
    const responseJson = await response.json()
    if (!responseJson) {
        return
    }
    const features = (localisationsSourceZoomedIn
        .getFormat() as any)
        .readFeatures(responseJson)

    localisationsSourceZoomedIn.addFeatures(features)

    return Promise.resolve()
}

export const localisationsSource = new VectorSource({
    format,
    strategy: tile(tileGrid),
    loader: localisationSourceLoader
}) as ol.source.Vector

export const localisationsSourceZoomedIn = new VectorSource({
    format,
    strategy: bbox, 
    loader: localisationZoomedInSourceLoader
}) as ol.source.Vector

export const clusterSource = new Cluster({
    source: localisationsSource,
    distance: CLUSTER_DISTANCE
})

export const localisations = new AnimatedCluster({
    name: 'localisations',
    maxResolution: MAX_RESOLUTION,
    maxZoom: NO_CACHE_AFTER_ZOOM,
    source: clusterSource,
    style: localisationsStyle
})

export const localisationsZoomedIn = new VectorLayer({
    name: 'localisations_zoomed_in',
    minZoom: NO_CACHE_AFTER_ZOOM,
    source: localisationsSourceZoomedIn,
    style: singleLocalisationStyle
})