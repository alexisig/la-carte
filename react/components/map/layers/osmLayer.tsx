import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM'

const osmLayer = new TileLayer({
    title: 'OpenStreetMap',
    source: new OSM()
} as any)

export default osmLayer