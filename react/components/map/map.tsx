import Map from 'ol/Map'
import LayerGroup from 'ol/layer/Group'
import osmLayer from '../map/layers/osmLayer'
import view from './view'
import { controls } from './controls'
import { localisations, localisationsZoomedIn } from './layers/localisations'
import geolocation from './layers/geolocation'

const map = new Map({
    layers: [
        new LayerGroup({
            name: 'basemap',
            layers: [ osmLayer ]
        }),
        new LayerGroup({
            name: 'vector',
            layers: [ geolocation, localisations, localisationsZoomedIn ]
        })
    ],
    controls,
    view
}) as ol.Map

export default map