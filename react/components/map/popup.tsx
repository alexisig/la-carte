import Overlay from 'ol/Overlay'

const getPopup = (opts) => new Overlay({
    ...opts, 
    offset: [15, 15]
})

export default getPopup