import { Fill, Stroke, Style, Circle, Text, Icon } from 'ol/style'
import VectorLayer from 'ol/layer/Vector'
import { NEGATIVE, POSITIVE, PROJET_MEDUSES, WHITE } from '../../styles/Colors'

const defaultStyle = new VectorLayer().getStyleFunction()()

const cacheStyle = {}

const negativeFeedbackStyle = new Style({
  image: new Circle({
    radius: 20,
    fill: new Fill({
      color: NEGATIVE,
      opacity: 0.6
    })
  })
})

const positiveFeedbackStyle = new Style({
  image: new Circle({
    radius: 20,
    fill: new Fill({
      color: POSITIVE,
      opacity: 0.6
    })
  })
})

const bakeryIconStyle = new Style({
  image: new Icon({
    anchor: [0.53, 120],
    anchorXUnits: 'fraction',
    anchorYUnits: 'pixels',
    src: 'https://img.icons8.com/cotton/2x/croissant--v1.png',
    scale: 0.15
  })
})

const repeatableIconStyle = new Style({
  image: new Icon({
    anchor: [-0.4, 350],
    anchorXUnits: 'fraction',
    anchorYUnits: 'pixels',
    src: 'https://img.icons8.com/pastel-glyph/2x/clock--v2.png',
    scale: 0.1
  })
})

export const singleLocalisationStyle = (feature) => {
  let iconStyle = null
  let baseStyle = null
  let repeatableStyle = null

  switch (feature.get('type')) {
    case 'bakery':
      iconStyle = bakeryIconStyle
      break;
    default:
      iconStyle = defaultStyle
  }
  
  const actionCollectionStatus = feature.get('action_collection_status') || []
  const { positive, negative, total } = actionCollectionStatus
    .reduce((acc, val) => {
      let positive = 0
      let negative = 0
      let total = 0

      const actionDones = val['action_done'] || []

      actionDones.forEach(actionDone => {
        if (!repeatableStyle && actionDone['repeatable_now'] === false) {
          repeatableStyle = repeatableIconStyle
        }
        if (actionDone['opportunity'] === true) {
          positive += 1
        } else if (actionDone['opportunity'] === false) {
          negative += 1
        }
        total += 1
      })

      acc.positive += positive
      acc.negative += negative
      acc.total += total

      return acc
    }, {
      positive: 0,
      negative: 0,
      total: 0
    })

  const sixtyPercent = total * 0.6
  const moreThan60PercentPositive = positive > sixtyPercent
  const moreThan60PercentNegative = negative > sixtyPercent

  if (moreThan60PercentPositive) {
    baseStyle = positiveFeedbackStyle
  } else if (moreThan60PercentNegative) {
    baseStyle = negativeFeedbackStyle
  }

  return [
    baseStyle,
    iconStyle,
    repeatableStyle
  ].filter(e => !!e)
}

const localisationsStyle = (feature, resolution) => {
    const size = feature.get('features').length
    const sizeKey = size.toString()

    if (size === 1) {
      return singleLocalisationStyle(feature.get('features')[0])
    } else if (sizeKey in cacheStyle) {
      return cacheStyle[sizeKey]
    }

    const style = new Style({
      image: new Circle({
        radius: size < 10 ? 10 : size > 50 ? 50 : size,
        stroke: new Stroke({
          color: WHITE,
        }),
        fill: new Fill({
          color: PROJET_MEDUSES.DARK_GREEN,
        }),
      }),
      text: new Text({
        text: size.toString(),
        fill: new Fill({
          color: WHITE,
        }),
      }),
    })

    cacheStyle[sizeKey] = style
   
    return style
}

export default localisationsStyle