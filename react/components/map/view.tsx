import View from 'ol/View'
import { fromLonLat } from 'ol/proj'

const parisPosition = fromLonLat([2.349014, 48.864716])

const view =  new View({
    center: parisPosition,
    zoom: 13
}) as ol.View

export default view