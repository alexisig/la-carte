import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { getActionCollectionStatus } from '../../store/models/localisation';
import { useState } from 'react';
import styled from 'styled-components';
import ActionDoneList from './ActionDoneList';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import ActionCollectionStepper from './ActionCollectionStepper';

const ActionCollectionCardWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    max-width: 100%;
    div {
        flex-grow: 1;
    }
`

const ActionCollectionTabsWrapper = styled.div`
    flex-basis: 100%;
`

const ActionCollectionTabs =  ({ tab, setTab, status }) => {
    const handleChange = (event: React.ChangeEvent<{}>, newValue: string) => {
      setTab(newValue);
    }
    
    const commentCounts = status
        .get('action_done', [])
        .filter(e => !!e.get('comment'))
        .size
    
    return (
        <ActionCollectionTabsWrapper>
            <Tabs onChange={handleChange} value={tab}>
                <Tab label='Actions' value='action-details' />
                {commentCounts != 0 && <Tab label={`Commentaire${commentCounts > 1 ? 's': ''} (${commentCounts})`} value='action-comments' />}
            </Tabs>
        </ActionCollectionTabsWrapper>
    )
}

export const ActionCollectionCard = ({ localisation, collection }) => {
    const status = getActionCollectionStatus(localisation, collection)
    const theme = useTheme()
    const IsMobile = useMediaQuery(theme.breakpoints.down('sm'));
    const [ tab, setTab ] = useState('action-details');

    if (!status) {
        return null
    }
    const [ isBeingValidated, setIsBeingValidated ] = useState(false)
    
    return (
        <>
        {IsMobile && <ActionCollectionTabs status={status} tab={tab} setTab={setTab} />}
            <ActionCollectionCardWrapper>
                {(!IsMobile || tab === 'action-details') && (
                    <Card className='action-details' variant='outlined'>
                        <CardContent>
                            <Typography variant="h6" gutterBottom>
                                {collection.get('name')}
                            </Typography>
                            <ActionCollectionStepper
                                localisation={localisation}
                                collection={collection}
                                status={status}
                                setIsBeingValidated={setIsBeingValidated}
                                isBeingValidated={isBeingValidated}
                            />
                        </CardContent>
                    </Card>
                )}
                {(!IsMobile || tab === 'action-comments') && status
                    .get('action_done', []).size > 0 && (
                            <>
                                &nbsp;&nbsp;
                                <ActionDoneList
                                    collection={collection}
                                    actionDone={status.get('action_done', [])}
                                />
                            </>
                        )
                }
            </ActionCollectionCardWrapper>
    </>
    )
}