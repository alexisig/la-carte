import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import StepActionDescription from './StepActionDescription';
import StepActionValidation from './StepActionValidation';

const sortActions = (a, b) => {
    return a.get('order_in_collection') > b.get('order_in_collection') ? 1 : -1
}

const getActiveStepIndex = (collection, status) => {
    return collection
        .get('actions')
        .findIndex(a => a.get('id') === status.get('current_action'))
}

const ActionCollectionStepper = ({
    localisation,
    collection,
    setIsBeingValidated,
    isBeingValidated,
    status
}) => (
    <Stepper activeStep={getActiveStepIndex(collection, status)} orientation='vertical'>
        {collection
            .get('actions')
            .sort(sortActions)
            .map(action => (
                <Step key={action.get('id')}>
                    <StepLabel>{action.get('name')}</StepLabel>
                    <StepContent>
                        {isBeingValidated ? (
                            <StepActionValidation
                                action={action}
                                localisation={localisation}
                                setIsBeingValidated={setIsBeingValidated}
                            />
                        ) : (
                            <StepActionDescription
                                status={status}
                                action={action}
                                localisation={localisation}
                                startValidating={setIsBeingValidated}
                            />
                        )}

                    </StepContent>
                </Step>
                )
            )
        }
    </Stepper>
)

export default ActionCollectionStepper