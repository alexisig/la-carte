import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography'
import { Fragment } from 'react';
import moment from 'moment'

const formatDate = (dateStr) => {
    return moment(dateStr).format("DD/MM/YYYY à hh:ss")
}

const ActionDoneList = ({ collection, actionDone }) => {
    const actions = collection.get('actions', [])
    const actionDoneFiltered = actionDone
        .filter(ad => actions
            .map(a => a.get('id'))
            .includes(ad.get('action'))
        )

    return (
        <Card style={{maxHeight: '50vh', overflow: 'scroll'}} variant='outlined'>
            <CardContent>
                {actionDoneFiltered
                    .reverse()
                    .map(actionDoneEvent => (
                        <Fragment key={actionDoneEvent.get('id')}>
                            <ListItem alignItems="flex-start">
                                <ListItemAvatar>
                                    <Avatar src={actionDoneEvent.getIn(['user', 'avatar'])} alt="Remy Sharp" />
                                </ListItemAvatar>
                                <ListItemText
                                    primary={actions
                                        .find(action => action.get('id') === actionDoneEvent.get('action'))
                                        .get('name')}
                                    secondary={
                                        <>
                                        <Typography
                                            component="span"
                                            variant="body2"
                                            color="textPrimary"
                                        >
                                            {formatDate(actionDoneEvent.get('date'))} par {actionDoneEvent.getIn(['user', 'username'])}
                                        </Typography>
                                            {!!actionDoneEvent.get('comment') && `- ${actionDoneEvent.get('comment')}`}
                                        </>
                                    }
                                    />
                            </ListItem>
                            <Divider />
                        </Fragment>
                    ))}
            </CardContent>
        </Card>
    )
}

export default ActionDoneList
