import React, { FC } from "react";
import { Box, Typography } from "@material-ui/core";

import ActionDone from "../../store/models/ActionDone";
import LocalisationSmallCard from "./LocalisationSmallCard";
import { OpportunityCard } from "../atoms/OpportunityCard";

interface Props {
  entry: ActionDone;
}

export const ExpandedActionDone: FC<Props> = ({ entry }) => {
    return (
      <Box display="flex" flexDirection="column" py={1} px={2} mb={1}>

        <Box display="flex" flexDirection="column" mb={2}>
          <Typography variant="overline">Description:</Typography>
          <Box p={1}>{ entry.action.description }</Box>
        </Box>

        <Box display="flex" flexDirection="column" mb={2}>
          <Typography variant="overline">Objectif:</Typography>
          <Box p={1}>{ entry.action.objective }</Box>
        </Box>

        <Box display="flex" flexDirection="column" mb={2}>
          <Typography variant="overline">Commentaire:</Typography>
          <Box p={1}>{ entry.comment }</Box>
        </Box>

        <Box display="flex" flexDirection="column" mb={2}>
          <Typography variant="overline">Appréciation:</Typography>
          <Box p={1}>
            <OpportunityCard opportunity={entry.opportunity} />
          </Box>
        </Box>

        <Box display="flex" flexDirection="column" mb={2}>
          <Typography variant="overline">Localisation:</Typography>
          <Box p={1}>
            <LocalisationSmallCard localisation={entry.localisation} showLink />
          </Box>
        </Box>
      </Box>
    );
  }
