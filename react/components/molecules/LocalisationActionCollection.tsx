import styled from 'styled-components'
import { getActionCollections, getActionCollectionStatus, getLocalisationActionProgress, getActionOrderInCollection } from '../../store/models/localisation'
import { ActionCollectionCard } from './ActionCollectionCard'

const ActionCollectionContainer = styled.div`
    display: flex;
    justify-content: row;
    flex-wrap: wrap;
`

const LocalisationActionCollection = ({ localisation }) => {

    const actionCollections = getActionCollections(localisation)
    
    if (!actionCollections) {
        return null
    }
    return (
        <ActionCollectionContainer>
            {actionCollections
                .map(collection => (
                    <ActionCollectionCard
                        localisation={localisation}
                        collection={collection}
                        key={collection.get('id')}
                    />
                )
            )}
        </ActionCollectionContainer>  
    )
}

export default LocalisationActionCollection