import { List } from "immutable"
import LocationOnIcon from '@material-ui/icons/LocationOn';
import styled from "styled-components";

const ADDR_TAGS = List([
    'housenumber',
    'street',
    'city',
    'postcode'
])

const AddressWrapper = styled.div`
    display: flex;
    align-items: center;
    flex-wrap: wrap;
`

const LocalisationAddress = ({ tags }) => {
    const hasAddressTags = tags
        .map((k, v) => v.includes('addr'))
        .toList()
        .toArray()
        .includes(true)
    
    if (!hasAddressTags) {
        return null
    }
    return (
        <AddressWrapper>
            <LocationOnIcon fontSize='small' />
            {ADDR_TAGS
                .filter(tag => !!tags.get(`addr:${tag}`))
                .map(tag => (
                <span key={tag}>{tags.get(`addr:${tag}`)}&nbsp;</span>
            ))}
        </AddressWrapper>
    )
}

export default LocalisationAddress