import { getName, getType, getActionCollections } from "../../store/models/localisation"
import LocalisationActionCollection from "./LocalisationActionCollection"
import Typography from '@material-ui/core/Typography'
import CircularProgress from '@material-ui/core/CircularProgress';
import LocalisationOpeningHours from "./LocalisationOpeningHours";
import LocalisationAddress from './LocalisationAddress'
import LocalisationChips from "./LocalisationChips";

export interface LocalisationCardPropsInterface {
    localisation: any;
}

const LocalisationCard = ({ localisation } : LocalisationCardPropsInterface) => {
    return (
        <>
            <Typography gutterBottom variant="h5">
                {getName(localisation)}&nbsp;
                    <Typography variant='subtitle1' component='div' display='inline' color='textSecondary'>
                        {getType(localisation)}
                    </Typography>
            </Typography>
            <LocalisationChips tags={localisation.getIn(['properties', 'tags'], [])} />
            <LocalisationAddress tags={localisation.getIn(['properties', 'tags'], [])} />
            <LocalisationOpeningHours tags={localisation.getIn(['properties', 'tags'], [])} />
            {!!getActionCollections(localisation) ? (
                <LocalisationActionCollection localisation={localisation} />
            ) : (
                <CircularProgress />
            )}
        </>
    )
}

export default LocalisationCard
