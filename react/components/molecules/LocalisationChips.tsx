import Chip from '@material-ui/core/Chip';
import AccessibleIcon from '@material-ui/icons/Accessible';
import { List, Map } from 'immutable';
import styled from 'styled-components';

const KEYS_ICONS = Map({
    'wheelchair': AccessibleIcon,
})

const KEYS_TRANSLATIONS = Map({
    'wheelchair': 'Accessible',
})

const TAGS_FOR_CHIPS = Map({
    'wheelchair': List(['yes'])
})

const ChipsWrapper = styled.div`
    .MuiChip-root {
        margin-right: 5px;
    }
`

const getChipsFromTags = (tags) => {
    return tags.filter((v, k) => {
        return (
            TAGS_FOR_CHIPS.has(k) &&    
            TAGS_FOR_CHIPS
                .get(k)
                .includes(v)
        )})
        .map((k, v) => KEYS_ICONS.get(v))
        .mapKeys(k => KEYS_TRANSLATIONS.get(k))
}

const LocalisationChips = ({ tags }) => {
    if (!tags) {
        return null
    }
    const chips = getChipsFromTags(tags)
    if (chips.size === 0) {
        return null
    }
    return (
        <ChipsWrapper>
            {chips.map((K, v) => (
                <Chip key={v}
                    variant='outlined'
                    icon={<K />}
                    label={v}
                />
            )).toList()}
            <br /><br />
        </ChipsWrapper>
    )

}

export default LocalisationChips