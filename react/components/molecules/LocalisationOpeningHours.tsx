import { fromJS, Map } from 'immutable'
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import styled from "styled-components";
import moment from 'moment'
import InfoIcon from '@material-ui/icons/Info';
import { getParsableOSMOpeningHours } from '../../utils/date';
import { Box } from '@material-ui/core';

const DAYS_TRANSLATIONS = Map({
    'su': 'Dimanche',
    'mo': 'Lundi',
    'tu': 'Mardi',
    'we': 'Mercredi',
    'th': 'Jeudi',
    'fr': 'Vendredi',
    'sa': 'Samedi',
    'ph': ''
})

const getTranslated = (map) => (key) => map.get(key, key)
const getTranslatedDay = getTranslated(DAYS_TRANSLATIONS)

interface HoursWrapperProps {
    isToday: boolean;
}

const HoursWrapper = styled.div<HoursWrapperProps>`
    display: flex;
    flex-basis: 100%;
    flex-direction: row;
    div:first-of-type {
        flex-grow: 1;
    }
    font-weight: ${({ isToday }) => isToday ? 'bold': 'normal'};
`

const HoursList = ({ value }) => {
    const days = fromJS(value.getTable())
        .filter((_, day) => day !== 'ph')
        .map((hours, day) => [
            getTranslatedDay(day),
            !!hours.first() ? hours.join(' - '): 'Fermé'
        ])
        .map(([day, hours]) => (
            <HoursWrapper isToday={moment()
                .locale('fr')
                .format('dddd') === day.toLowerCase()
            } key={day}>
                <div>{day}</div>
                <div>{hours}</div>
            </HoursWrapper>
        ))
        .toList()
    
    const daysReordered = days
        .push(days.first())
        .shift()

    return daysReordered
}

const LocalisationOpeningHoursWrapper = styled.div`
    .MuiAccordionDetails-root {
        flex-wrap: wrap;
    }
`

const CovidSpecificHoursWrapper = styled.div`
    display: flex;
    align-items: center;
    padding: 15px 0px;
    margin-bottom: 15px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.12);
    border-top: 1px solid rgba(0, 0, 0, 0.12);
    .MuiSvgIcon-root {
        margin-right: 15px;
    }
`

const LocalisationOpeningHours = ({ tags }: { tags: any }) => {
    const { hours, areHoursCovid19Specific } = getParsableOSMOpeningHours(tags)

    if (!hours) {
        return null
    }

    const isOpen = hours.isOpenNow()
    const summaryLabel = isOpen ? 'Ouvert Maintenant' : 'Fermé'

    return (
        <LocalisationOpeningHoursWrapper>
            <Accordion variant='outlined' elevation={1}>
                <AccordionSummary style={{color: isOpen ? '#4caf50' : '#f44336'}} expandIcon={<ExpandMoreIcon />}>
                    <AccessTimeIcon fontSize='small' />&nbsp;{summaryLabel}
                </AccordionSummary>
                <AccordionDetails>
                    {areHoursCovid19Specific && (
                        <CovidSpecificHoursWrapper>
                            <InfoIcon fontSize='large' color='primary' />&nbsp;
                            <b>
                                Les horaires de ce lieu sont spécifiques à la période COVID 19  
                                et peuvent ne pas correspondre à la réalité.
                            </b>
                        </CovidSpecificHoursWrapper>
                    )}
                    <HoursList value={hours} />
                </AccordionDetails>
            </Accordion>
            <br />
        </LocalisationOpeningHoursWrapper>
    )
}

export default LocalisationOpeningHours
