import { FC } from 'react';
import Link from 'next/link'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

import { getName, getType } from "../../store/models/localisation"

interface Props {
    localisation: any;
    showLink?: boolean;
}

const LocalisationSmallCard: FC<Props> = ({ localisation, showLink }) => {
    return (
        <Card>
            <CardContent>
                <Box display="flex" flexDirection="row" alignItems="center" justifyContent="space-between">
                    <Box>
                        <Typography variant="h5">{getName(localisation)}</Typography>
                        <Typography variant="h6" gutterBottom>
                                {getType(localisation)}
                        </Typography>
                    </Box>
                    { showLink && (
                        <Link href={`/localisations/${localisation.id}`}>
                            <Button>Voir</Button>
                        </Link>
                    )}
                </Box>
            </CardContent>
        </Card>
    )
}

export default LocalisationSmallCard
