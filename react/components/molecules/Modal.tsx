import styled from "styled-components"
import { setClickedFeature } from "../../store/actions"
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

interface ModalInterface {
    children: JSX.Element;
}

const InnerModal = styled.div`
    width: calc(100vw - 15px);
    position: absolute;
    top: 5vh;
    left: 50%;
    transform: translateX(-50%);
    right: 0px;
    bottom: 0px;
    overflow-y: scroll;
`

const OuterModal = styled.div`
    width: 100%;
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.15);
`

const onOuterModalClick = (e) => {
    const { id } = e.target
    if (id !== 'modal-overlay') {
        return
    }
    setClickedFeature(null)
}

const Modal = ({ children } : ModalInterface) => {
    return (
        <OuterModal id='modal-overlay' onClick={onOuterModalClick}>
            <InnerModal>
                <Card>
                    <CardContent>
                        {children}
                    </CardContent>
                </Card>
            </InnerModal>
        </OuterModal>
    )
}

export default Modal