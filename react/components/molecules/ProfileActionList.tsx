import React, { FC, useState } from 'react'
import { List } from 'immutable'
import { Box, Collapse, List as ListContainer, ListItem, ListItemText, Paper, Typography } from '@material-ui/core';
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'

import ActionDone from '../../store/models/ActionDone'
import { formatDate } from '../../utils/date';
import { ExpandedActionDone } from './ExpandedActionDone';

interface Props {
  entries: List<ActionDone>;
}

export const ProfileActionList: FC<Props> = ({ entries }) => {
    const [expanded, setExpanded] = useState(null);

    return (
      <ListContainer>
        { entries.map((entry) => {
          const entryExpanded = expanded === entry;
          return (
            <Box key={entry.id + Math.random()} mb={1}>
              <Paper>
                <ListItem onClick={() => setExpanded(expanded !== entry ? entry : null)}>
                  <ListItemText>
                    <Box display="flex" justifyContent="space-between" alignItems="center">
                      {entry.action.name}
                      <Typography variant="caption">{ formatDate(entry.date) }</Typography>
                    </Box>
                    </ListItemText>
                  { entryExpanded ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={entryExpanded} unmountOnExit>
                  { entryExpanded && <ExpandedActionDone entry={expanded} /> }
                </Collapse>
              </Paper>
            </Box>
          );
        }) }
      </ListContainer>
    );
  }
