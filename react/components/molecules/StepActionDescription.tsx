import { connect } from 'react-redux'
import { List } from 'immutable'
import { minutesIntegerToFutureDate } from '../../store/dates-util';
import { getLocalisationActionProgress, isActionRepeatableNow } from '../../store/models/localisation';
import LinearProgress from '@material-ui/core/LinearProgress';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import styled from 'styled-components';
import Alert from '@material-ui/lab/Alert';

const StepActionDesriptionWrapper = styled.div``

const StepActionDescription = ({ 
    localisation, 
    startValidating, 
    action, 
    status, 
    user 
}) => {
    const { repeatableNow, nextCompletionTime } = isActionRepeatableNow(action, localisation, user, status)
    
    const [ done, todo ] = getLocalisationActionProgress(localisation, action)
    const percentDone = done * 100 / todo
    const userHasPermissionToComplete = action.get('user_has_permission_to_complete')
    const permissionToCompleteGroups = action.get('permission_to_complete') as List<string>

    const permissionToCompleteIsDefault = (
        permissionToCompleteGroups.size === 1 &&
        permissionToCompleteGroups.first() === '@everyone'
    )

    return (
        <StepActionDesriptionWrapper>
            <p>{action.get('description')}</p>
            <LinearProgress
                value={percentDone}
                variant='determinate'
            />
            <p>{action.get('objective')}</p>
            <p>{done} / {todo}</p>
            {userHasPermissionToComplete && (
                <ButtonGroup orientation="vertical">
                    <Button disabled>Signaler un problème</Button>
                    <Button disabled={!repeatableNow} onClick={startValidating} variant="contained" color='primary'>Valider cette action</Button>
                </ButtonGroup>
            )}
            <br /><br />
            {!permissionToCompleteIsDefault && (
                <Alert color='info'>
                    La réalisation de cette action est réservée aux
                    membres {permissionToCompleteGroups.size > 1 ? (
                        <>des groupes :</>
                    ) : (
                        <>du groupe :</>
                    )} {permissionToCompleteGroups.join(', ')}
                    
                    {permissionToCompleteGroups.size === 0 && 'Admin'}
                    {/* TODO: reuse role chips from the user profile */}
                </Alert>
            )}
            {!repeatableNow && (
                <>
                    <br />
                    <Alert icon={<AccessTimeIcon fontSize="inherit" />}  color='success'>
                        <b>Merci d'avoir effectué cette action !</b>&nbsp;
                        Elle sera à nouveau disponible pour cette localisation {minutesIntegerToFutureDate(nextCompletionTime).fromNow()}.
                    </Alert>
                </>
            )}
        </StepActionDesriptionWrapper>
    )
}

export default connect(store => ({ 
    user: store.get('user') 
}))(StepActionDescription)