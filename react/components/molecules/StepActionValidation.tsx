import { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import CircularProgress from '@material-ui/core/CircularProgress';
import { createActionDone, updateClickedFeatureInStoreById } from '../../store/actions';
import Slider from '@material-ui/core/Slider';
import styled from 'styled-components';
import Alert from '@material-ui/lab/Alert';

const SliderWrapper = styled.div`
    .MuiSlider-root {
        margin-left: 30px;
        width: calc(100% - 60px);
    }
`

const SLIDER_MARKS = [
    {
        value: 0,
        label: 'Négative',
        db_value: false
    }, {
        value: 50,
        label: 'Neutre',
        db_value: null,
    }, {
        value: 100,
        label: 'Positive',
        db_value: true
    }
]

const StepActionValidationForm = styled.form``

const StepActionValidation = ({ action, localisation, setIsBeingValidated }) => {
    const [ comment, setComment ] = useState('')
    const [ submitting, setSubmitting ] = useState(false)
    const [ opportunityRating, setOpportunityRating ] = useState(50)

    const onCommentChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setComment(event.target.value);
    }

    return (
        <StepActionValidationForm noValidate autoComplete="off">
            <p>
                Laisser un commentaire utile permet 
                aux autres participants de mieux
                participer.
            </p>
            <TextField
                label="Commentaire"
                multiline
                rows={4}
                variant="outlined"
                value={comment}
                onChange={onCommentChange}
                autoFocus
            />
            <p>
                Mon expérience était:
            </p>
            <SliderWrapper>
                <Slider
                    value={opportunityRating}
                    onChange={(e, value) => setOpportunityRating(value as number)}
                    aria-labelledby="discrete-slider-restrict"
                    track={false}
                    step={null}
                    marks={SLIDER_MARKS}
                />
            </SliderWrapper>
            {opportunityRating === 0 && (
                <Alert severity="error">Mon expérience était mauvaise et je ne pense pas que cette action peut fonctionner.</Alert>
            )}
            {opportunityRating === 50 && (
                <Alert severity="info">Mon expérience de cette action n'était ni positive ni négative.</Alert>
            )}
            {opportunityRating === 100 && (
                <Alert severity="success">Mon expérience était bonne et je pense que cette action peut fonctionner.</Alert>
            )}
            <br />
            {submitting ? (
                <CircularProgress />
            ) : (
                <ButtonGroup
                    color="primary"
                    fullWidth
                    aria-label="outlined primary button group">
                    <Button onClick={() => setIsBeingValidated(false)}>Annuler</Button>
                    <Button
                        disabled={submitting}
                        onClick={async (e) => {
                            e.preventDefault();
                            setSubmitting(true)
                            await createActionDone(
                                comment,
                                action.get('id'),
                                localisation.get('id'), 
                                SLIDER_MARKS.find(e => e.value === opportunityRating).db_value
                            )
                            await new Promise(resolve => setTimeout(() => {
                                resolve(setIsBeingValidated(false))
                            }, 250))
                            await updateClickedFeatureInStoreById(localisation.get('id'))
                        }}
                        variant="contained"
                        color='primary'>
                            Valider
                    </Button>
                </ButtonGroup>
            )}
        </StepActionValidationForm>
    )
}

export default StepActionValidation