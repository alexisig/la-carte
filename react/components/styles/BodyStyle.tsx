import { createGlobalStyle } from "styled-components"
import { PROJET_MEDUSES, WHITE } from "./Colors"

const BodyStyle = createGlobalStyle`
  body {
    margin: 0;
    background: linear-gradient(
            135deg,
            ${PROJET_MEDUSES.RED} 5%,
            ${PROJET_MEDUSES.DARK_GREEN} 45%,
            ${PROJET_MEDUSES.DARK_GREEN} 55%,
            ${PROJET_MEDUSES.CYAN} 95%
        );
    * {
      font-family: 'Roboto';
    }
  }
`

export default BodyStyle
