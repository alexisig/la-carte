export const GREY = '#CDCDCD'
export const WHITE = '#fff'
export const BLACK = '#333'

export const POSITIVE = '#4caf50'
export const NEGATIVE = '#f44336'

export const PROJET_MEDUSES = {
    RED: '#f66760',
    DARK_GREEN: '#143f57',
    CYAN: '#07dfc9'
}