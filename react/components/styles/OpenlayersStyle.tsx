import { createGlobalStyle } from "styled-components"
import { BLACK, PROJET_MEDUSES, WHITE } from "./Colors"

const OpenlayersStyle = createGlobalStyle`
    #map {
        .ol-zoom {
            background-color: transparent;
            padding: 1px;
            box-shadow: 0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.1),0px 1px 10px 0px rgba(0,0,0,0.05);
            left: initial;
            top: 1em;
            right: 1em;
            button {
                background-color: ${WHITE};
                color: ${BLACK};
                font-weight: normal;
            }
        }
    }
`

export default OpenlayersStyle
