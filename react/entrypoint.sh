npm install --production
npm run build

if [ -n "$1" ]
then
  if [ "$1" == "dev" ]
  then
    npm run dev
  elif [ "$1" == "prod" ]
  then
    npm run start
  fi
fi