import { useEffect, useState } from "react";
import { useIntersect } from "./useIntersect";

export const useInfiniteScroll = () => {
    const [loaderElement, setLoaderElement] = useState<HTMLElement>(null);
    const [setNode, entry] = useIntersect();

    useEffect(() => {
        if (loaderElement) setNode(loaderElement);
    }, [loaderElement])

    return [setLoaderElement, !!entry?.isIntersecting] as const;
}
