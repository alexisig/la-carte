import { useEffect, useRef, useState } from "react";

export const useIntersect = ({ root, rootMargin, threshold }: IntersectionObserverInit = {}) => {
    const [entry, setEntry ] = useState<IntersectionObserverEntry>(null);
    const [node, setNode] = useState<HTMLElement>(null);

    const observer = useRef<IntersectionObserver>(null);

    useEffect(() => {
        if (observer.current) observer.current.disconnect();
        observer.current = new IntersectionObserver(([entry]) => setEntry(entry), { root, rootMargin, threshold });
        const { current: currentRef } = observer;
        if (node) currentRef.observe(node);
        return () => currentRef.disconnect();
    }, [node, root, rootMargin, threshold]);

    return [setNode, entry] as const;
}
