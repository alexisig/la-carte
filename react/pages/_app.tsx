import * as React from "react";
import { Provider } from "react-redux";
import App, { AppProps } from "next/app";
import store from "../store/store";
import BodyStyle from "../components/styles/BodyStyle";
import Layout from "../components/layouts/Layout";
import { getJWTTokenFromLocalStorage } from "../store/jwt";
import { setUser } from "../store/actions";
import Head from "next/head";
import CssBaseline from '@material-ui/core/CssBaseline';

const EVERY_FIVE_MINUTES = 5 * 60 * 1000

class LaCarteApp extends App {
    props: AppProps;
    refreshTokenInterval: any;
    static async getInitialProps({ Component, ctx }) {
        const pageProps = Component.getInitialProps ?
            await Component.getInitialProps(ctx) : {};
        return { pageProps };
    }

    refreshCallback = async () => {
        const token = getJWTTokenFromLocalStorage()
        const { refreshToken } = require('../store/jwt')
        if (token) {
            try {
                const { user } = await refreshToken(token)
                setUser(user)
            } catch (error) {
                console.error(error)
            }
        } else {
            setUser()
        }
    }

    componentDidMount() {
        this.refreshCallback()
        this.refreshTokenInterval = setInterval(this.refreshCallback, EVERY_FIVE_MINUTES)
    }

    componentWillUnmount() {
        clearInterval(this.refreshTokenInterval)
    }

    render() {
        const { Component, pageProps } = this.props;

        return (
            <Provider store={store}>
                <Head>
                    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
                </Head>
                <CssBaseline />
                <BodyStyle />
                <Layout>
                    <Component {...pageProps} />
                </Layout>
            </Provider>
        );
    }
}

export default LaCarteApp