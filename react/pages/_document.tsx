import Document from 'next/document'
import { ServerStyleSheet as StyledComponentsSSRSheets } from 'styled-components'
import { ServerStyleSheets as MaterialUISSRSheets } from '@material-ui/core/styles'

export default class MyDocument extends Document {
  static async getInitialProps (ctx) {
    const StyledComponentsSheets = new StyledComponentsSSRSheets()
    const MaterialUISheets = new MaterialUISSRSheets()
    const originalRenderPage = ctx.renderPage

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: App => props => (
            StyledComponentsSheets.collectStyles(
              MaterialUISheets.collect(<App {...props} />)
            )
          )
        })

      const initialProps = await Document.getInitialProps(ctx)
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {StyledComponentsSheets.getStyleElement()}
            {MaterialUISheets.getStyleElement()}
          </>
        )
      }
    } finally {
      StyledComponentsSheets.seal()
    }
  }
}
