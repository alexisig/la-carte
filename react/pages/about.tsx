import React from 'react'
import styled from 'styled-components'

const About = ({ className }) => (
  <div className={className}>About</div>
)

export default styled(About)``
