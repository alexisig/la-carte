import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'
import { connect } from 'react-redux'
import User from '../store/models/User'

const HomeWrapper = styled.div``

interface HomePropsInterface {
  user: User
}

const HomeLink = ({ href, label }) => (
  <li>
    <Link href={href}>
      <a>{label}</a>
    </Link>
  </li>
)

const Home = ({ user }: HomePropsInterface) => (
  <HomeWrapper>
    {user.isAuthenticated() && (
      <ul>
       <HomeLink href='/map' label='Accèder aux actions sur la carte' />
       <HomeLink
          href='https://discord.com/channels/613310664164769792/668876026805485609'
          label='Participer à la conception des actions et de la campagne sur discord'
        />
       <HomeLink
          href='https://projet-meduses.jetbrains.space/p/web-int/code/la_carte'
          label="Participer au dévelopment de l'application sur Space" />
       <HomeLink
          href='https://docs.google.com/document/d/1CWDnwOdh5tWLpya9kkFZ5lxHv6GYGLbXGaLGOHytDgY/edit#'
          label="Documentation sur la création d'action"
        />
      </ul>
    )}
  </HomeWrapper>
)

export default connect((store) => ({
  user: store.get('user')
}))(Home)
