import { Component } from 'react'
import { connect } from 'react-redux'
import { setClickedFeature } from '../../store/actions'
import CircularProgress from '@material-ui/core/CircularProgress';
import { LocalisationCardNoSSR } from '../../components/dynamics/dynamics';

interface LocalisationsByIdProps {
    id: string;
    clicked_feature?: any;
}

class LocalisationsById extends Component<LocalisationsByIdProps> {
    static async getInitialProps({ query }) {
        const { id } = query
        return { id }
    }

    componentDidMount() {
        const { id, clicked_feature } = this.props
        if(!clicked_feature) {
            setClickedFeature(id)
        }
    }

    render() {
        const { clicked_feature } = this.props
        if (!clicked_feature) {
            return <CircularProgress />
        }
        return  <LocalisationCardNoSSR localisation={clicked_feature} />
    }
}

const ConnectedLocalisationsById = connect((store) => ({
    clicked_feature: store.getIn(['map', 'clicked_feature'])
  }))(LocalisationsById)

ConnectedLocalisationsById.displayName = 'localisation'

export default ConnectedLocalisationsById