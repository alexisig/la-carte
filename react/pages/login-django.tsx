import redirect from 'nextjs-redirect'

const Redirect = redirect(process.env.NEXT_PUBLIC_LOGIN_DJANGO_REDIRECT)

const LoginDjango = () => (
  <Redirect>
    <div>Redirection vers Discord ...</div>
  </Redirect>
)

export default LoginDjango