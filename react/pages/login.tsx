import React from "react"
import fetch from 'cross-fetch'
import { saveToken } from "../store/jwt"
import { setUser } from "../store/actions"
import Router from 'next/router'
import Link from "next/link"

interface LoginPagePropsInterface {
    data: {
        token: string;
        user: object
    }
}

export default class LoginPage extends React.Component<LoginPagePropsInterface> {
    static async getInitialProps({ query }) {
        const { code } = query

        let data = {}

        if (code) {
            const requestUrl = `${process.env.NEXT_PRIVATE_DJANGO_BASE_URL}/auth/token/exchange`
            const djangoResponse = await fetch(requestUrl, { 
                headers: { "Content-Type": "application/json; charset=UTF-8" }, 
                method: 'POST', 
                body: JSON.stringify({ code })
            })
            data = await djangoResponse.json()
        }

        return { data }
    }

    componentDidMount() {
        const { data } = this.props
        if (!data) {
            return
        }
        const { token, user } = data
        if (!token || !user) {
            return
        }
        saveToken(token)
        setUser(user)
        setTimeout(() => Router.push('/map'), 0)
    }

    isUserSuccessfullyLoggedIn() {
        const { data } = this.props
        if (!data) {
            return false
        }
        const { token } = data
        return !!token
    }

    render() {
        return (
            <div>{this.isUserSuccessfullyLoggedIn() ? (
                <>
                    <p>Connexion réussie.</p>
                    <p>Redirection ...</p>
                </>
            ): (
                <>
                    <p>Connexion échouée.</p>
                    <p>Avez vous créé un compte ? Si non, vous pouvez le faire <Link href="/signup"><a>ici</a></Link>.</p>
                    <p>Si cela ne résoud pas le problème, postez sur le channel <b>#la-carte</b> sur discord.</p>
                </>
            )}</div>
        )
    }
}