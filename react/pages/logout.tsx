import { useEffect } from "react"
import { setUser } from "../store/actions";
import { fakeLogout } from "../store/jwt";

const LogoutPage = () => {
    useEffect(() => { 
        fakeLogout()
        setUser()
      });
    return (
        <div>Deconnexion réussie</div>
    )
}

export default LogoutPage