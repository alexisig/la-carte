import { Component } from 'react'
import Head from 'next/head'
import { MapContainerNoSSR } from '../components/dynamics/dynamics'
import { connect } from "react-redux";
import redirect from 'nextjs-redirect'
import { getJWTTokenFromLocalStorage } from '../store/jwt';
import Position from '../store/models/position';
import OpenlayersStyle from '../components/styles/OpenlayersStyle';

interface MapPageInterface {
  initialPosition?: Position;
  clicked_feature?: any;
}

const Redirect = redirect('/')

class MapPage extends Component<MapPageInterface> {
  render() {
    const { clicked_feature, initialPosition } = this.props

    if (typeof window !== 'undefined' && !getJWTTokenFromLocalStorage()) {
      return (
        <Redirect>
          <div>Redirection ...</div>
        </Redirect>
      )
    }

    return (
      <div>
        <Head>
          <link rel="stylesheet" href="//cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.1.1/css/ol.css" type="text/css" />
        </Head>
        <OpenlayersStyle />
        <MapContainerNoSSR initialPosition={initialPosition} />
      </div>
    )
  }
}

const ConnectedMapPage = connect((store) => ({
  clicked_feature: store.getIn(['map', 'clicked_feature']),
}))(MapPage)

ConnectedMapPage.displayName = 'map'

export default ConnectedMapPage