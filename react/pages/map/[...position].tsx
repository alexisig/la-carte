import { NextRouter } from 'next/router'
import Position from '../../store/models/position'
import MapPage from '../map'
import { Component } from 'react'

interface MapPageWithPositionProps {
    initialPosition?: Position
    router?: NextRouter;
}

class MapPageWithPosition extends Component<MapPageWithPositionProps> {
    static displayName = 'map'
    static async getInitialProps({ query }) {
        const { position } = query

        if (!position || !Array.isArray(position) || position.length !== 3) {
            return { initialPosition: null }
        }

        const [ lon, lat, zoom ] = position.map(Number)
        return { initialPosition:  new Position({ lat, lon, zoom }) }
    }

    render() {
        const { initialPosition } = this.props
        return <MapPage initialPosition={initialPosition} />
    }
}

export default MapPageWithPosition