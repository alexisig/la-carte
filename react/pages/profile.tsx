import React, { FC, useEffect, useRef, useState } from 'react'
import { useSelector } from "react-redux";
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Chip from '@material-ui/core/Chip';

import { getUserActionDones, setUserHistoryLoading } from '../store/actions';
import User from '../store/models/User';
import { AdminBadge } from '../components/atoms/AdminBadge';
import { ProfileActionList } from '../components/molecules/ProfileActionList';
import { useInfiniteScroll } from '../hooks/useInfiniteScroll';
import { SkeletonList } from '../components/atoms/Skeletons';

interface Props {
  user: User;
}

const Profile: FC<Props> = () => {
  const user: User = useSelector(store => store.get('user'));
  const [page, setPage] = useState(1);

  const [loaderRef, shouldLoadMore] = useInfiniteScroll();

  const fetchActionDones = async (page = 1) => {
    try {
      setUserHistoryLoading(true);
      await getUserActionDones(page);
      setUserHistoryLoading(false);
    } catch (err) {
      setUserHistoryLoading(false);
    }
  }

  useEffect(() => {
    fetchActionDones();
  }, []);

  useEffect(() => {
    if (shouldLoadMore) {
      fetchActionDones(page + 1);
      setPage(page + 1)
    }
  }, [shouldLoadMore]);

  return (
    <>
      <Box display="flex" flexDirection="row" alignItems="center" justifyContent="space-between">
        <Typography variant="h4">{user.username}</Typography>
        {user.is_campaigner && <AdminBadge />}
      </Box>
      <Box my={1}>
        {user.groups.map(group => (
          <Box key={group} display="inline-block" mr={.5} mb={.5}>
            <Chip
              variant="outlined"
              size="small"
              label={group}
            />
          </Box>
        ))}
      </Box>
      <Box mt={5}>
        <Typography variant="h5">Actions réalisées</Typography>
        <ProfileActionList entries={user.history.action_dones} />
        {user.history.has_more && (
          <div ref={loaderRef}>
            <SkeletonList count={5} />
          </div>
        )}
      </Box>
    </>
  )
}

export default Profile
