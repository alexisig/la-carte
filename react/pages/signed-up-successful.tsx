import Link from "next/link";

const SignedUpSuccessfulPage = () => (
    <div>Vous avez crée votre compte. Vous pouvez maintenant vous <Link href="/login-django"><a>connecter</a></Link>.</div>
)

export default SignedUpSuccessfulPage