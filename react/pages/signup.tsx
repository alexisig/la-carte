import redirect from 'nextjs-redirect'

const Redirect = redirect(`${process.env.NEXT_PUBLIC_DJANGO_BASE_URL}/accounts/discord/login/`)

export default () => (
    <Redirect>
      <div>Redirection vers Django ...</div>
    </Redirect>
  )