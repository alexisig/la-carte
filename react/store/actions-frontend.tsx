import { mutate } from "./actions"
import * as actions from "./action-types"
import GeoJSON from 'ol/format/GeoJSON'
import ImmutableGeoJSON from 'immutable-geojson'

export const setResultsFromFeatures = (features) => {
    const featuresRecord = ImmutableGeoJSON.fromJS(new GeoJSON().writeFeaturesObject(features))
    mutate(() => featuresRecord, actions.SET_RESULTS)
}