import store from './store'
import * as actions from './action-types'
import ImmutableGeoJSON from 'immutable-geojson'
import { getFeatureInStoreById } from './utils'
import fetch from 'cross-fetch'
import { getFetchRequest } from './jwt'
import User from './models/User'
import { UserActionDoneHistory } from "./models/UserActionDoneHistory"
import Position from './models/position'
import LocalisationType from './models/LocalisationType'
import { List, Map } from 'immutable'
import ActionDone, { IActionDone } from './models/ActionDone'
import Action from './models/Action'
import { Paginated } from '../utils/types'

export const mutate = (fn, type) => store.dispatch({ type, fn })

export const setPosition = (position: Position) => {
    mutate(() => position, actions.SET_POSITION)
}



export const setUser = (user: any = {}) => {
    mutate(() => new User({ ...user, groups: List(user.groups) }), actions.SET_USER)
    if (Object.keys(user).length > 0) {
        getAndSetLocalisationTypes()
    }
}

export const setInGeolocation = (obj) => {
    mutate(geolocation => geolocation.merge(obj), actions.UPDATE_GEOLOCATION)
}

export const setInMapProperties = (setkey, value) => {
    mutate(map => map.set(setkey, value), actions.SET_MAP_PROPERTY)
}

export const getSingleLocalisation = async (featureId) => {
    const requestUrl = `${process.env.NEXT_PUBLIC_DJANGO_BASE_URL}/api/localisations/${featureId}`
    const response = await getFetchRequest(requestUrl)
    const asJson = await response.json()
    return ImmutableGeoJSON.fromJS(asJson)
}

export const updateClickedFeatureInStoreById = async (featureId) => {
    setInMapProperties('hovered_feature', null)
    if (!featureId) {
        setInMapProperties('clicked_feature', null)
        return
    }
    const record = await getSingleLocalisation(featureId)
    setInMapProperties('clicked_feature', record)
    return record
}

export const setHoveredFeature = (featureId) => {
    const state = store.getState()
    const previousHoveredFeature = state.getIn(['map', 'hovered_feature'])
    const featureRecord = getFeatureInStoreById(featureId)

    if (!previousHoveredFeature && !featureRecord) {
        return
    }

    if (previousHoveredFeature && featureRecord && previousHoveredFeature.id === featureRecord.id) {
        return
    }

    setInMapProperties('hovered_feature', featureRecord)
}

export const setClickedFeature = async (featureId) => {
    const featureRecord = getFeatureInStoreById(featureId)
    setInMapProperties('clicked_feature', featureRecord)
    const completeFeatureRecord = await updateClickedFeatureInStoreById(featureId)
    return completeFeatureRecord
}

export const createActionDone = async (comment, actionId, localisationId, opportunity) => {
    const requestUrl = `${process.env.NEXT_PUBLIC_DJANGO_BASE_URL}/api/action_dones/`
    const response = await getFetchRequest(requestUrl, {
        method: 'POST',
        body: JSON.stringify({
            comment,
            opportunity,
            action: actionId,
            localisation: localisationId
        })
    })
    return response.json()
}

export const getAndSetLocalisationTypes = async () => {
    const requestUrl = `${process.env.NEXT_PUBLIC_DJANGO_BASE_URL}/api/localisations_types/`
    const response = await getFetchRequest(requestUrl)
    const rawLocalisationTypes = await response.json()
    const localisationTypes = rawLocalisationTypes
        .reduce((acc, rawLocalisationType) => {
            return acc.set(
                rawLocalisationType['name'],
                new LocalisationType(rawLocalisationType)
            )
        }, Map())
    mutate(() => localisationTypes, actions.SET_LOCALISATOIN_TYPE)
}

export const setUserHistoryLoading = (loading: boolean) => {
    mutate((user: User) => {
        const { count, has_more, action_dones } = user.history;
        const history = new UserActionDoneHistory({ count, has_more, action_dones, loading });
        return user.set('history', history);
    }, actions.SET_USER_HISTORY_LOADING);
}

export const getUserActionDones = async (page: number = 1) => {
    const requestUrl = `${process.env.NEXT_PUBLIC_DJANGO_BASE_URL}/api/user/action_dones?page=${page}`
    try {
        const response = await getFetchRequest(requestUrl)
        const raw = await response.json() as Paginated<IActionDone>;
        const actionDones = raw.results.map(actionDone => new ActionDone({
            ...actionDone,
            action: new Action(actionDone.action),
            localisation: ImmutableGeoJSON.fromJS(actionDone.localisation),
        }));
        mutate((user: User) => {
            const action_dones = page === 1 ? List(actionDones) : user.history.action_dones.concat(actionDones);
            const history = new UserActionDoneHistory({ count: raw.count, action_dones, has_more: !!raw.next, loading: false });
            return user.set('history', history)
        }, actions.SET_USER_ACTIONS_DONE);
    } catch (err) {
        console.error(err);
        // Handle failed request
    }
}
