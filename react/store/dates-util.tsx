import moment from 'moment'

export const minutesIntegerToFutureDate = (minutes: number) => {
    return moment()
        .add(minutes, 'minutes')
        .locale('fr')
}

