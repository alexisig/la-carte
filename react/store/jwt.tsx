export const getJWTTokenFromLocalStorage = () => {
    const token = localStorage.getItem('token')
    return !!token ? token : ''
}

export const isLoggedIn = () => {
    return !!getJWTTokenFromLocalStorage()
}

export const saveToken = (token) => {
    localStorage.setItem('token', token)
}

export const fakeLogout = () => {
    localStorage.removeItem('token')
}
 
export const getFetchRequest = (url: string, opts? : RequestInit) => {
    if (!opts) {
        opts = {}
    }
    let postMethodHeader = {}
    if ('method' in opts && opts.method === 'POST') {
        postMethodHeader = { "Content-Type": "application/json" }
    }
    return fetch(url, {
        ...opts,
        headers: {
            'Authorization': 'JWT ' + getJWTTokenFromLocalStorage(),
            ...postMethodHeader
        }
    })
}

export const refreshToken = async (oldToken) => {
    const refreshUrl = `${process.env.NEXT_PUBLIC_DJANGO_BASE_URL}/auth/token/refresh`
    const response = await getFetchRequest(refreshUrl, {
        method: 'POST',
        body: JSON.stringify({ token: oldToken })
    })
    if (response.status !== 200) {
        fakeLogout()
        return
    }
    const responseJson = await response.json()
    const { token, user } = responseJson
    saveToken(token)
    return { token , user }
}