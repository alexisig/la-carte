import { Record } from 'immutable'

class Action extends Record({
    id: -1,
    collection: -1,
    delay_before_repeat: 0,
    description: "",
    name: "",
    objective: "",
    order_in_collection: 0,
    publication_date: "",
    required_completion_to_end: 0,
    start_condition: "",
}) { }

export default Action
