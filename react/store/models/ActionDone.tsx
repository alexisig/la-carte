import { Record } from 'immutable'
import Action from './Action'
import User from './User'

export interface IActionDone {
    id: number;
    comment: string;
    date: string;
    action: Action;
    localisation: any;
    opportunity: boolean | null;
    repeatable_now: boolean;
    time_until_next_possible_completion: number;
    user: User | null;
}

class ActionDone extends Record<IActionDone>({
    id: -1,
    comment: "",
    date: "",
    action: null as Action,
    localisation: null,
    opportunity: false,
    repeatable_now: false,
    time_until_next_possible_completion: 0,
    user: null,
}) { }

export default ActionDone
