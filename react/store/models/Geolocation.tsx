import { Record } from 'immutable'

class Geolocation extends Record({ 
    coordinates: null,
    accuracy: -1, // m
    accuracyGeometry: null,
    heading: -1, // rad,
    spped: -1 // m/s
}) { }

export default Geolocation