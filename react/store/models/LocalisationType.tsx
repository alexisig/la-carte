import { Record } from 'immutable'

class LocalisationType extends Record({ 
    id: -1,
    name: '',
    display_name: '',
    description: '',
}) { }

export default LocalisationType