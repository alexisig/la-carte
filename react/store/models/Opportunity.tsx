import { Record } from 'immutable'
import { Color } from '@material-ui/lab/Alert';

class Opportunity extends Record({
    id: -1,
    color: "" as Color,
    message: "",
    value: null,
}) { }

export const OPPORTUNITIES_STUB = [
    { id: 1, color: 'error', message: "Mon expérience était mauvaise et je ne pense pas que cette action peut fonctionner.", value: false },
    { id: 2, color: 'info', message: "Mon expérience de cette action n'était ni positive ni négative.", value: null },
    { id: 3, color: 'success', message: "Mon expérience était bonne et je pense que cette action peut fonctionner.", value: true },
].map(_ => new Opportunity({ ..._, color: _.color as Color }))


export default Opportunity
