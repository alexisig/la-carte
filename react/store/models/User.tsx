import { List, Record } from 'immutable'
import { UserActionDoneHistory } from './UserActionDoneHistory'

class User extends Record({
    id: -1,
    username: '',
    avatar: '',
    groups: List(),
    is_campaigner: false,
    history: new UserActionDoneHistory(),
}) {
    isAuthenticated() {
        return !!this.get('username') && !!this.get('id')
    }
}

export default User
