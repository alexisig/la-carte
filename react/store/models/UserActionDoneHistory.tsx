import { List, Record } from 'immutable';
import ActionDone from './ActionDone';

export class UserActionDoneHistory extends Record({
    count: 0,
    loading: false,
    action_dones: List<ActionDone>(),
    has_more: false,
}) {
}
