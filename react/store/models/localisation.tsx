import store from "../store"

const getInLocalisationTags = (localisation, key, alternative) => {
    const tags = localisation.properties.get('tags')
    return tags.get(key, alternative)
}

export const getName = (localisation) => {
    return getInLocalisationTags(localisation, 'name', localisation.properties.get('osm_id'))
}

export const getType = (localisation) => {
    const typeFromFeature = localisation.properties.get('type')
    return store
        .getState()
        .getIn([
            'localisations_types',
            typeFromFeature,
            'display_name'
        ], typeFromFeature)
}

export const getActionCollections = (localisation) => {
    return localisation.properties.get('action_collections')
}

export const getActionCollectionStatus = (localisation, collection) => {
    return localisation.properties
        .get('action_collection_status', [])
        .find(status => status.get('action_collection') === collection.get('id'))
}

export const getLocalisationActionProgress = (localisation, action) => {
    const requiredToCompletion = action.get('required_completion_to_end')
    const actionDoneEventsLength = localisation.properties
        .get('action_collection_status')
        .find(status => status.get('action_collection') === action.get('collection'))
        .get('action_done')
        .filter(actionDoneEvent => actionDoneEvent.get('action') === action.get('id'))
        .size
    
    return [
        actionDoneEventsLength,
        requiredToCompletion
    ]
}

export const getActionOrderInCollection = (action, collection) => {
    return [
        action.get('order_in_collection'),
        collection.get('actions').size
    ]
}

export const isActionRepeatableNow = (action, localisation, user, status) => {
    const firstActionInstatus = status
        .get('action_done', [])
        .filter(actionDone => (
            actionDone.get('action') === action.get('id') &&
            actionDone.get('localisation') === localisation.get('id') &&
            actionDone.getIn(['user', 'id']) === user.get('id')
        ))
        .first()

    const repeatableNow = firstActionInstatus ? firstActionInstatus.get('repeatable_now') : true
    const nextCompletionTime = firstActionInstatus ? firstActionInstatus.get('time_until_next_possible_completion') : 0

    return {
        repeatableNow,
        nextCompletionTime
    }
}