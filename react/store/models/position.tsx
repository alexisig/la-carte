import { Record } from 'immutable'

class Position extends Record({ 
    lat: -1,
    lon: -1,
    zoom: -1
}) {
    isReady() {
        return (
            this.lat !== -1 &&
            this.lon !== -1 &&
            this.zoom !== -1
        )
    }
}

export default Position