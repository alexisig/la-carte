import { createStore } from 'redux'
import { composeWithDevTools } from "redux-devtools-extension"
import { Map, List } from 'immutable'
import * as actions from './action-types'
import ImmutableGeoJSON from 'immutable-geojson'
import User from './models/User'
import Geolocation from './models/Geolocation'
import { OPPORTUNITIES_STUB } from './models/Opportunity'

const initialState = Map({
  user: new User(),
  position: null,
  geolocation: new Geolocation(),
  results: ImmutableGeoJSON.fromJS({}),
  localisations_types: Map(),
  map: Map({
    hovered_feature: null,
    clicked_feature: null
  }),
  opportunities: List(OPPORTUNITIES_STUB),
});

interface ActionInterface {
  fn?: Function;
  type: string;
}

const reduce = (state = initialState, action: ActionInterface) => {
  if (!action.fn) {
      return state
  }

  switch (action.type) {
      case actions.SET_RESULTS:
          return state.set('results', action.fn(state, action))
      case actions.SET_MAP_PROPERTY:
          return state.set('map', action.fn(state.get('map'), action))
      case actions.SET_USER:
          return state.set('user', action.fn(state.get('user'), action))
      case actions.SET_POSITION:
          return state.set('position', action.fn(state.get('position'), action))
      case actions.UPDATE_GEOLOCATION:
          return state.set('geolocation', action.fn(state.get('geolocation'), action))
      case actions.SET_LOCALISATOIN_TYPE:
          return state.set('localisations_types', action.fn(state.get('localisations_types'), action))
      case actions.SET_USER_ACTIONS_DONE:
          return state.set('user', action.fn(state.get('user'), action))
      case actions.SET_USER_HISTORY_LOADING:
          return state.set('user', action.fn(state.get('user'), action))
      default:
          return state
  }
}

const devTools = (process.browser && window['__REDUX_DEVTOOLS_EXTENSION__'])
  ? window['__REDUX_DEVTOOLS_EXTENSION__']()
  : f => f

const composeEnhancers = composeWithDevTools({
  trace: true,
  traceLimit: 25,
})

const store = createStore(reduce, initialState, composeEnhancers())

export default store
