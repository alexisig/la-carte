import store from "./store"

export const getFeatureInStoreById = (featureId) => {
    const results = store
        .getState()
        .get('results')
    if (!results) {
        return 
    }
    const featureRecord = results
        .features
        .find(feature => feature.id === featureId)
    return featureRecord
}