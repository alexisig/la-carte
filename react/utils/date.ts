import { SimpleOpeningHours } from "simple-opening-hours"
import { List, Map } from 'immutable'
import moment from 'moment'
import 'moment/locale/fr'

export const formatDate = (date: string | Date) => moment(date).format('DD MMMM YYYY')

export const USE_COVID_HOURS = true

export const OPENING_HOURS_OSM_KEY = 'opening_hours'
export const OPENING_HOURS_COVID_19_OSM_KEY = 'opening_hours:covid19'

const OPENING_HOURS_COVID_19_SPECIAL_VALUES = Map({
    'sameHoursAsRegular': List(['*', 'open', 'same']),
    'unspecifiedChanges': List(['restricted', 'varie']),
    'closed': List(['off', 'closed'])
})

export const getParsableOSMOpeningHours = (tags) : {
    hours: SimpleOpeningHours,
    areHoursCovid19Specific: Boolean
} => {
    let hours = null
    let areHoursCovid19Specific = false

    const openingHours = tags.find((_, key) => key === OPENING_HOURS_OSM_KEY)
    const openingHoursCovid19 = tags.find((_, key) => key === OPENING_HOURS_COVID_19_OSM_KEY)

    const placeHasNoHours = !openingHours && !openingHoursCovid19

    if (placeHasNoHours) {
        return { hours, areHoursCovid19Specific }
    }

    const shouldUseRegularOpeningHours = OPENING_HOURS_COVID_19_SPECIAL_VALUES
        .get('sameHoursAsRegular')
        .includes(openingHoursCovid19)

    const shouldUseCovid19Hours = (
        !shouldUseRegularOpeningHours &&
        !!openingHoursCovid19 &&
        USE_COVID_HOURS
    ) || OPENING_HOURS_COVID_19_SPECIAL_VALUES
        .get('closed')
        .includes(openingHoursCovid19)


    if (shouldUseCovid19Hours) {
        try {
            hours = new SimpleOpeningHours(openingHoursCovid19)
            areHoursCovid19Specific = true
        } catch {
            console.error(`Couldn't parse ${OPENING_HOURS_COVID_19_OSM_KEY} opening hours for this place.`)
        }
    }

    if (!hours && openingHours) {
        try {
            hours = new SimpleOpeningHours(openingHours)
        } catch {
            console.error(`Couldn't parse ${OPENING_HOURS_OSM_KEY} opening hours for this place.`)
        }
    }

    return { hours, areHoursCovid19Specific }
}
